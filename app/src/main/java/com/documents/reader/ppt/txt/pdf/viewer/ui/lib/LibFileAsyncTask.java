package com.documents.reader.ppt.txt.pdf.viewer.ui.lib;

import android.content.Context;
import android.os.AsyncTask;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.ui.search.SearchFileListener;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.util.ArrayList;

public class LibFileAsyncTask extends AsyncTask<Object, Object, Object> {

    private final Context mContext;
    private SearchFileListener mListener;
    private int mOrder;
    private String mFilterType;

    public LibFileAsyncTask(Context context, SearchFileListener listener, int order, String filterType) {
        mContext = context;
        mListener = listener;
        this.mOrder = order;
        this.mFilterType = filterType;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            ArrayList<FileData> allData = FileUtils.getAllExternalFileList(mContext, mFilterType, mOrder);

            if (!isCancelled() && mListener != null) {
                mListener.loadDone(allData);
            }
        } catch (Exception e) {
            mListener.loadDone(new ArrayList<>());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
