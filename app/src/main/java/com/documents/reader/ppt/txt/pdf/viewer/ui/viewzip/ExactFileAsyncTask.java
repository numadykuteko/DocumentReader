package com.documents.reader.ppt.txt.pdf.viewer.ui.viewzip;

import android.content.Context;
import android.os.AsyncTask;

import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

public class ExactFileAsyncTask extends AsyncTask<Object, Object, Object> {

    private final Context mContext;
    private ExactFileListener mListener;
    private String mFilePath;

    public ExactFileAsyncTask(Context context, ExactFileListener listener, String filePath) {
        mContext = context;
        mListener = listener;
        this.mFilePath = filePath;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            String outputPath = FileUtils.exactFile(mContext, mFilePath);

            if (!isCancelled() && mListener != null) {
                mListener.loadDone(outputPath);
            }
        } catch (Exception e) {
            mListener.loadDone(null);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
