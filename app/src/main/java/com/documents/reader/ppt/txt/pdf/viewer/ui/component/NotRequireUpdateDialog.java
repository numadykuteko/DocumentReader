package com.documents.reader.ppt.txt.pdf.viewer.ui.component;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class NotRequireUpdateDialog extends BottomSheetDialogFragment {
    private ConfirmListener mListener;
    private String mVersion;

    public NotRequireUpdateDialog() {
        // Required empty public constructor
    }

    public NotRequireUpdateDialog(String version, ConfirmListener listener) {
        this.mListener = listener;
        mVersion = version;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.sheet_dialog_style);
    }

    private void setAutoExpanded() {
        if (getDialog() != null) {
            getDialog().setOnShowListener(dialog -> {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                View bottomSheetInternal = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                if (bottomSheetInternal != null) {
                    BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            });
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_update_not_require, container, false);

        TextView versionTv = v.findViewById(R.id.version_info);
        versionTv.setText(getString(R.string.update_version_info, mVersion));

        Button submitBtn = v.findViewById(R.id.btn_ok);
        submitBtn.setOnClickListener(v1 -> {
            if (mListener != null)
                mListener.onSubmit();

            dismiss();
        });

        return v;
    }

    public interface ConfirmListener {
        void onSubmit();
    }
}
