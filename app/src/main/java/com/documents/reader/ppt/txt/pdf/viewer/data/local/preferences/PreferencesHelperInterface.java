package com.documents.reader.ppt.txt.pdf.viewer.data.local.preferences;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.ViewPdfOption;

public interface PreferencesHelperInterface {
    int getOpenBefore();
    void setOpenBefore(int opened);

    int getRatingUs();
    void setRatingUs(int rated);

    void setShowGuideConvert(int shown);
    int getShowGuideConvert();

    void setShowGuideSelectMulti(int shown);
    int getShowGuideSelectMulti();

    int getBackTime();
    void setBackTime(int time);

    String getAppLocale();
    void setAppLocale(String locale);

    ViewPdfOption getViewPDFOptions();
    void saveViewPDFOptions(ViewPdfOption viewPdfOption);

    void setTheme(int theme);
    int getTheme();

    int getOpenFromOtherAppTime();
    void setOpenFromOtherAppTime(int time);
}
