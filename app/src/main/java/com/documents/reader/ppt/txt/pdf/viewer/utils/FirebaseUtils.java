package com.documents.reader.ppt.txt.pdf.viewer.utils;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
public class FirebaseUtils {
    public static final String EVENT_TYPE = "event_type";

    public static final String EVENT_NAME_RATE = "prox_rating_layout";
    public static final String EVENT_NAME_HOME = "prox_home_layout";
    public static final String EVENT_NAME_LIST_FILE = "prox_list_file_layout";

    public static void sendEventFunctionUsed(Context context, String eventName, String eventType) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(context);

        Bundle bundle = new Bundle();
        bundle.putString(EVENT_TYPE, eventType);
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void sendEventFunctionUsed(Context context, String eventName, String eventType, String comment) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(context);

        Bundle bundle = new Bundle();
        bundle.putString(EVENT_TYPE, eventType);
        firebaseAnalytics.logEvent(eventName, bundle);
    }
}
