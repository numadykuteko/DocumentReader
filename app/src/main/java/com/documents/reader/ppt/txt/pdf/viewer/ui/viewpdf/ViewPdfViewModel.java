package com.documents.reader.ppt.txt.pdf.viewer.ui.viewpdf;

import android.app.Application;

import androidx.annotation.NonNull;

import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;

public class ViewPdfViewModel extends BaseViewModel<ViewPdfNavigator> {
    public ViewPdfViewModel(@NonNull Application application) {
        super(application);
    }
}
