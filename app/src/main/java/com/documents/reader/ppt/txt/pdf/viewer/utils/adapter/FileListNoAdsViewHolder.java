package com.documents.reader.ppt.txt.pdf.viewer.utils.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.listener.OnFileItemClickListener;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DateTimeUtils;

public class FileListNoAdsViewHolder extends RecyclerView.ViewHolder {
    private ImageView mImageView;
    private TextView mNameView;
    private TextView mDateTextView;
    private ConstraintLayout mContentView;

    public FileListNoAdsViewHolder(@NonNull View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        mContentView = itemView.findViewById(R.id.item_content_view);
        mImageView = itemView.findViewById(R.id.item_image_view);
        mNameView = itemView.findViewById(R.id.item_name_view);
        mDateTextView = itemView.findViewById(R.id.item_date_text_view);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void bindView(int position, FileData fileData, int currentItem, OnFileItemClickListener listener) {
        mNameView.setText(fileData.getDisplayName());
        if (fileData.getTimeAdded() > 0) {
            mDateTextView.setVisibility(View.VISIBLE);
            mDateTextView.setText(DateTimeUtils.fromTimeUnixToDateString(fileData.getTimeAdded()));
        } else {
            mDateTextView.setVisibility(View.GONE);
        }

        switch (fileData.getFileType()) {
            case DataConstants.FILE_TYPE_PDF:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_pdf));
                break;
            case DataConstants.FILE_TYPE_EXCEL:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_excel));
                break;
            case DataConstants.FILE_TYPE_WORD:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_word));
                break;
            case DataConstants.FILE_TYPE_TXT:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_txt));
                break;
            case DataConstants.FILE_TYPE_PPT:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_ppt));
                break;
            case DataConstants.FILE_TYPE_CSV:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_csv));
                break;
            case DataConstants.FILE_TYPE_RTF:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_rtf));
                break;
            case DataConstants.FILE_TYPE_RAR:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_rar));
                break;
            case DataConstants.FILE_TYPE_ZIP:
                mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_zip));
                break;
        }

        mContentView.setOnClickListener(v -> {
            listener.onClickItem(position);
        });
    }
}
