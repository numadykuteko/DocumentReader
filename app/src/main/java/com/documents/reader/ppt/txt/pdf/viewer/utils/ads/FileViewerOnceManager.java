package com.documents.reader.ppt.txt.pdf.viewer.utils.ads;

import android.util.Log;

public class FileViewerOnceManager {
    private static final String LOG = "FileViewerOnceManager";

    private static FileViewerOnceManager mInstance;
    private boolean mIsFileViewerOnce;

    private FileViewerOnceManager() {
        mIsFileViewerOnce = false;
    }

    public static FileViewerOnceManager getInstance() {
        if (mInstance == null) {
            mInstance = new FileViewerOnceManager();
        }

        return mInstance;
    }


    public boolean checkFileViewerOnce() {
        Log.d("duynm", mIsFileViewerOnce + "");
        return mIsFileViewerOnce;
    }

    public void setFileViewerOnce() {
        mIsFileViewerOnce = true;
    }

}
