package com.documents.reader.ppt.txt.pdf.viewer.utils.scheduler;

import io.reactivex.Scheduler;

public interface SchedulerProviderInterface {

    Scheduler computation();

    Scheduler io();

    Scheduler ui();
}
