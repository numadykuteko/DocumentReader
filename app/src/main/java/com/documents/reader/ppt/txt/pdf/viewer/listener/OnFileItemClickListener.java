package com.documents.reader.ppt.txt.pdf.viewer.listener;

public interface OnFileItemClickListener {
    void onClickItem(int position);
}
