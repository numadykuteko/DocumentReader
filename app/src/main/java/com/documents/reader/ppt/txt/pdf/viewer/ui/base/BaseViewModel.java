package com.documents.reader.ppt.txt.pdf.viewer.ui.base;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.documents.reader.ppt.txt.pdf.viewer.data.DataManager;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtilAsyncTask;
import com.documents.reader.ppt.txt.pdf.viewer.utils.scheduler.SchedulerProvider;
import com.documents.reader.ppt.txt.pdf.viewer.utils.scheduler.SchedulerProviderInterface;

import java.lang.ref.WeakReference;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseViewModel<N> extends AndroidViewModel implements FileUtilAsyncTask.FileListener {

    private DataManager mDataManager;
    private CompositeDisposable mCompositeDisposable;
    private WeakReference<N> mNavigator;
    private ObservableBoolean mIsLoading = new ObservableBoolean();
    private SchedulerProviderInterface mSchedulerProviderInterface;

    private MutableLiveData<List<FileData>> mListFileSelectorLiveData = new MutableLiveData<>();
    public MutableLiveData<List<FileData>> getListFileSelectorLiveData() {
        return mListFileSelectorLiveData;
    }

    public BaseViewModel(@NonNull Application application) {
        super(application);
        mCompositeDisposable = new CompositeDisposable();
        mSchedulerProviderInterface = new SchedulerProvider();

        mDataManager = DataManager.getInstance(application);
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
        super.onCleared();
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public SchedulerProviderInterface getSchedulerProvider() {
        return mSchedulerProviderInterface;
    }

    public void setDataManager(DataManager appDataManager) {
        this.mDataManager = appDataManager;
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    public N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
    }

    public void getFileList(String fileType, int order) {
        FileUtilAsyncTask asyncTask = new FileUtilAsyncTask(getApplication(), this, order, fileType);
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void saveRecent(String filePath, String type) {
        getCompositeDisposable().add(getDataManager()
                .saveRecent(filePath, type)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {

                }, throwable -> {

                })
        );
    }

    public void saveRecent(RecentData recentData) {
        getCompositeDisposable().add(getDataManager()
                .saveRecent(recentData)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {

                }, throwable -> {

                })
        );
    }

    public void clearRecent(String filePath) {
        getCompositeDisposable().add(getDataManager()
                .clearRecent(filePath)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {

                }, throwable -> {

                })
        );
    }

    public void clearSavedData(String filePath) {
        try {
            clearRecent(filePath);
        } catch (Exception ignored) {

        }
    }

    public void updateSavedData(String filePath, String newFilePath) {

        getCompositeDisposable().add(getDataManager()
                .getRecentByPath(filePath)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.getFilePath().equals(filePath)) {
                        clearRecent(filePath);
                        saveRecent(newFilePath, response.getActionType());
                    }
                }, throwable -> {

                })
        );
    }

    @Override
    public void loadDone(List<FileData> result) {
        mListFileSelectorLiveData.postValue(result);
    }
}
