package com.documents.reader.ppt.txt.pdf.viewer.utils.file;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.utils.pdf.PdfUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DirectoryUtils {
    private Context mContext;
    private ArrayList<String> mFilePaths;

    public DirectoryUtils(Context context) {
        mContext = context;
        mFilePaths = new ArrayList<>();
    }

    public ArrayList<String> getAllDocumentsOnDirectory(File directory, String filterType) {
        mFilePaths = new ArrayList<>();
        List<String> extensions = new ArrayList<>();
        if (filterType.equals(DataConstants.FILE_TYPE_ALL_DOCUMENT)) {
            extensions = DataConstants.ALL_EXTENSIONS;
        } else {
            extensions.add(filterType);
        }
        File[] listFile = directory.listFiles();
        if (listFile != null) {
            for (File aListFile : listFile) {

                if (aListFile.isDirectory()) {
                    // don't go away
                } else {
                    for (String extension: extensions) {
                        if (aListFile.getName().toLowerCase().endsWith(extension)) {
                            //Do what ever u want
                            mFilePaths.add(aListFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        return mFilePaths;
    }

    ArrayList<String> getAllDocumentsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), DataConstants.ALL_EXTENSIONS);
        return mFilePaths;
    }

    ArrayList<String> getAllPDFsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Collections.singletonList(DataConstants.PDF_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllExcelsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Arrays.asList(DataConstants.EXCEL_EXTENSION, DataConstants.EXCEL_WORKBOOK_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllCsvOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Collections.singletonList(DataConstants.EXCEL_CSV_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllPptsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Arrays.asList(DataConstants.PPT_EXTENSION, DataConstants.PPTX_EXTENSION, DataConstants.POT_EXTENSION, DataConstants.PPTM_EXTENSION, DataConstants.POTX_EXTENSION, DataConstants.POTM_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllRTFOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Collections.singletonList(DataConstants.RTF_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllRarOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Arrays.asList(DataConstants.RAR_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllZipOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Collections.singletonList(DataConstants.ZIP_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllWordsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Arrays.asList(DataConstants.DOC_EXTENSION, DataConstants.DOCX_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllTextsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Arrays.asList(DataConstants.DOC_EXTENSION, DataConstants.DOCX_EXTENSION, DataConstants.TEXT_EXTENSION));
        return mFilePaths;
    }

    ArrayList<String> getAllTxtsOnDevice() {
        mFilePaths = new ArrayList<>();
        walkDir(Environment.getExternalStorageDirectory(), Collections.singletonList(DataConstants.TEXT_EXTENSION));
        return mFilePaths;
    }

    private void walkDir(File dir, List<String> extensions) {
        File[] listFile = dir.listFiles();
        if (listFile != null) {
            for (File aListFile : listFile) {

                if (aListFile.isDirectory()) {
                    walkDir(aListFile, extensions);
                } else {
                    for (String extension: extensions) {
                        if (aListFile.getName().toLowerCase().endsWith(extension)) {
                            //Do what ever u want
                            mFilePaths.add(aListFile.getAbsolutePath());
                            break;
                        }
                    }
                }
            }
        }
    }

    public static String getDefaultStorageLocation() {
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), DataConstants.PDF_DIRECTORY);
        if (!dir.exists()) {
            boolean isDirectoryCreated = dir.mkdir();
            if (!isDirectoryCreated) {
                Log.e("Error", "Directory could not be created");
            }
        }
        return dir.getAbsolutePath() + "/";
    }
}
