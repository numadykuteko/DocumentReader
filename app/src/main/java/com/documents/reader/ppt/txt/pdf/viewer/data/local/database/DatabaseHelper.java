package com.documents.reader.ppt.txt.pdf.viewer.data.local.database;

import android.content.Context;

import androidx.room.Room;

import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;

import java.util.List;

import io.reactivex.Observable;

public class DatabaseHelper implements DatabaseHelperInterface {
    private static DatabaseHelper mInstance;
    private final ApplicationDatabase mApplicationDatabase;

    private DatabaseHelper(Context context) {
        mApplicationDatabase = Room.databaseBuilder(context, ApplicationDatabase.class, DataConstants.DATABASE_NAME).fallbackToDestructiveMigration()
                .build();
    }

    public static DatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            return new DatabaseHelper(context);
        }
        return mInstance;
    }

    @Override
    public Observable<Boolean> saveRecent(RecentData recentData) {
        return Observable.fromCallable(() -> {
            recentData.setTimeAdded(System.currentTimeMillis() / 1000L);
            mApplicationDatabase.recentDataDao().insert(recentData);
            return true;
        });
    }

    @Override
    public Observable<List<RecentData>> getListRecent() {
        return mApplicationDatabase.recentDataDao().loadAll().toObservable();
    }

    @Override
    public Observable<Boolean> clearRecent(String filePath) {
        return Observable.fromCallable(() -> {
            mApplicationDatabase.recentDataDao().deleteByPath(filePath);
            return true;
        });
    }

    @Override
    public Observable<RecentData> getRecentByPath(String filePath) {
        return mApplicationDatabase.recentDataDao().findByPath(filePath).toObservable();
    }
}
