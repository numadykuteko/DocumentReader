package com.documents.reader.ppt.txt.pdf.viewer.constants;

public class AppConstants {
    public static final String LOG_APP = "OFFICE_READER";

    public static final int FLAG_VIEW_PDF = 3;
    public static final int FLAG_SEARCH_PDF = 12;
 }
