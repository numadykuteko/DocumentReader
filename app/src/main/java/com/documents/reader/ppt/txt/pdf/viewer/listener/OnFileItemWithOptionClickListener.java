package com.documents.reader.ppt.txt.pdf.viewer.listener;

public interface OnFileItemWithOptionClickListener {
    void onClickItem(int position);
    void onMainFunctionItem(int position);
    void onOptionItem(int position);
}
