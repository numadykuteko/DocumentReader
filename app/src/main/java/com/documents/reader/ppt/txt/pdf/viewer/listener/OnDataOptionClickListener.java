package com.documents.reader.ppt.txt.pdf.viewer.listener;

public interface OnDataOptionClickListener {
    void onClickItem(int position);
}
