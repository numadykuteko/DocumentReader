package com.documents.reader.ppt.txt.pdf.viewer.ui.folder;

import android.content.Context;
import android.os.AsyncTask;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.ui.search.SearchFileListener;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.util.ArrayList;

public class FolderFileAsyncTask extends AsyncTask<Object, Object, Object> {
    private FileData mCurrentPath;
    private final Context mContext;
    private SearchFileListener mListener;
    private int mSort;
    private String mType;

    public FolderFileAsyncTask(Context context, SearchFileListener listener, FileData currentPath, int sort, String type) {
        mContext = context;
        mListener = listener;
        mCurrentPath = currentPath;
        mSort = sort;
        mType = type;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            ArrayList<FileData> allData;
            if (mCurrentPath == null) {
                allData = FileUtils.getNotEmptyFolderList(mContext);
            } else {
                allData = FileUtils.getAllFileOfDirectory(mContext, mCurrentPath, mSort, mType);
            }

            if (!isCancelled() && mListener != null) {
                mListener.loadDone(allData);
            }
        } catch (Exception e) {
            mListener.loadDone(new ArrayList<>());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
