package com.documents.reader.ppt.txt.pdf.viewer.ui.main;

import android.app.Application;

import androidx.annotation.NonNull;

import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;

public class MainViewModel extends BaseViewModel<MainNavigator> {
    public MainViewModel(@NonNull Application application) {
        super(application);
    }
}
