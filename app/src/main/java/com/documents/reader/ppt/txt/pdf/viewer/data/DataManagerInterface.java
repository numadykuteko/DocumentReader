package com.documents.reader.ppt.txt.pdf.viewer.data;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.ViewPdfOption;

import java.util.List;

import io.reactivex.Observable;

public interface DataManagerInterface {
    boolean isOpenBefore();
    void setOpenBefore();

    // rate :
    void setRatingUsDone();
    boolean checkRatingUsDone();

    void setShowGuideConvertDone();
    boolean getShowGuideConvert();

    void setShowGuideSelectMultiDone();
    boolean getShowGuideSelectMulti();

    int getBackTime();
    void increaseBackTime();

    String getAppLocale();
    void setAppLocale(String locale);

    ViewPdfOption getViewPDFOptions();
    void saveViewPDFOptions(ViewPdfOption viewPdfOption);

    void setTheme(int theme);
    int getTheme();

    int getOpenFromOtherAppTime();
    void setOpenFromOtherAppTime(int time);

    Observable<Boolean> saveRecent(RecentData recentData);
    Observable<List<RecentData>> getListRecent();
    Observable<Boolean> saveRecent(String filePath, String type);
    Observable<Boolean> clearRecent(String filePath);
    Observable<RecentData> getRecentByPath(String filePath);
}
