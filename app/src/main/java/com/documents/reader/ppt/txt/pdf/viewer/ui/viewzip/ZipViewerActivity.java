package com.documents.reader.ppt.txt.pdf.viewer.ui.viewzip;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ads.control.Admod;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.ActivityViewZipBinding;
import com.documents.reader.ppt.txt.pdf.viewer.listener.OnFileItemWithOptionClickListener;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseBindingActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ColorUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DialogFactory;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.adapter.ZipFileAdapter;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.RealPathUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ZipViewerActivity extends BaseBindingActivity<ActivityViewZipBinding, ZipViewerViewModel> implements ZipViewerNavigator, OnFileItemWithOptionClickListener {
    private ActivityViewZipBinding mActivityViewZipBinding;
    private ZipViewerViewModel mZipViewerViewModel;

    private boolean mIsLoading;
    private List<FileData> mListFile = new ArrayList<>();
    private ZipFileAdapter mFileListAdapter;

    private String mFilePath;
    private String mFileName;

    private String mZipPath;
    private FileData mCurrentFolder;

    private SweetAlertDialog mUnzipDialog;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_view_zip;
    }

    @Override
    public ZipViewerViewModel getViewModel() {
        mZipViewerViewModel = ViewModelProviders.of(this).get(ZipViewerViewModel.class);
        return mZipViewerViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mZipViewerViewModel.setNavigator(this);
        mActivityViewZipBinding = getViewDataBinding();

        String extraFilePath = getIntent().getStringExtra(EXTRA_FILE_PATH);
        if (extraFilePath != null && extraFilePath.length() > 0 && FileUtils.checkFileExist(extraFilePath)) {
            mFileName = FileUtils.getFileName(extraFilePath);
            mFilePath = extraFilePath;
        }

        initView();
    }

    @Override
    protected void initView() {
        Admod.getInstance().loadBanner(this, BuildConfig.banner_id);

        mActivityViewZipBinding.toolbar.toolbarBtnBack.setColorFilter(getIconColor(), android.graphics.PorterDuff.Mode.MULTIPLY);
        mActivityViewZipBinding.toolbar.toolbarActionShare.setColorFilter(getIconColor(), android.graphics.PorterDuff.Mode.MULTIPLY);

        mActivityViewZipBinding.toolbar.toolbarNameTv.setText(mFileName);
        mActivityViewZipBinding.toolbar.toolbarActionShare.setOnClickListener(v -> {
            FileUtils.shareFile(this, new File(mFilePath));
        });
        mActivityViewZipBinding.toolbar.toolbarBtnBack.setOnClickListener(v -> {
            super.onBackPressed();
            finish();
        });

        mUnzipDialog = DialogFactory.getDialogProgress(this, getString(R.string.exacting_file));
        mUnzipDialog.show();
        showLoadingArea();
        mZipViewerViewModel.getZipPathLiveData().observe(this, this::exactDone);
        mZipViewerViewModel.startExactZipFile(mFilePath);
    }

    private void exactDone(String exactPath) {
        if (mUnzipDialog != null && mUnzipDialog.isShowing()) {
            mUnzipDialog.dismiss();
        }

        if (FileUtils.checkFileExist(exactPath)) {
            mZipPath = exactPath;

            mCurrentFolder = new FileData();
            mCurrentFolder.setFilePath(mZipPath);

            mZipViewerViewModel.getListFileLiveData().observe(this, this::updateData);

            mFileListAdapter = new ZipFileAdapter(this);
            mActivityViewZipBinding.dataListArea.setLayoutManager(new LinearLayoutManager(this));
            mActivityViewZipBinding.dataListArea.setAdapter(mFileListAdapter);

            mActivityViewZipBinding.pullToRefresh.setOnRefreshListener(() -> {
                reloadData(false);
            });

            if (!mFilePath.contains(BuildConfig.APPLICATION_ID)) {
                mZipViewerViewModel.saveRecent(mFilePath, "Read zip file");
            }

            reloadData(true);
        } else {
            SweetAlertDialog sweetAlertDialog = DialogFactory.getDialogError(this, getString(R.string.exacting_file_error));
            sweetAlertDialog.setConfirmClickListener(sweetAlertDialog1 -> {
                sweetAlertDialog.dismiss();
                gotoViewByAndroidViewActivity(mFilePath, FileUtils.getFileType(new File(mFilePath)));
                onBackPressed();
            });
            sweetAlertDialog.show();
        }
    }

    @SuppressLint("SetTextI18n")
    public void reloadData(boolean isForceReload) {

        if (mIsLoading) return;

        mIsLoading = true;

        if (mListFile == null || mListFile.size() == 0 || isForceReload) {
            showLoadingArea();
        }

        if (mCurrentFolder != null) {
            if (mCurrentFolder.getFilePath().equals(mZipPath)) {
                mActivityViewZipBinding.fileDir.setText("");
                mActivityViewZipBinding.directoryArea.setVisibility(View.GONE);
            } else {
                mActivityViewZipBinding.fileDir.setText(mCurrentFolder.getFilePath().substring(mCurrentFolder.getFilePath().indexOf(mFileName)));
                mActivityViewZipBinding.directoryArea.setVisibility(View.VISIBLE);
            }

            mZipViewerViewModel.setCurrentPath(mCurrentFolder);
            mZipViewerViewModel.getFileList();
        }

    }

    private int getIconColor() {
        return ColorUtils.getColorFromResource(this, R.color.icon_type_day_mode);
    }

    @Override
    protected void setClick() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    public void onBackPressed() {
        if (mCurrentFolder != null && !mCurrentFolder.getFilePath().equals(mZipPath)) {
            if (mCurrentFolder.getParentFile() != null && mCurrentFolder.getParentFile().getFilePath() != null && mCurrentFolder.getParentFile().getFilePath().length() > 0) {
                if (mIsLoading) return;

                File dir = new File(mCurrentFolder.getParentFile().getFilePath());
                if (dir.isDirectory()) {
                    FileData temp = mCurrentFolder.getParentFile();
                    mCurrentFolder = new FileData(temp);

                    reloadData(false);
                }
            }
        } else {
            super.onBackPressed();
            finish();
        }
    }

    private void updateData(List<FileData> fileDataList) {
        if (fileDataList.size() > 0) {
            if (fileDataList.equals(mListFile)) {
                mIsLoading = false;
                mActivityViewZipBinding.pullToRefresh.setRefreshing(false);

                return;
            }

            mListFile = new ArrayList<>();
            mListFile.addAll(fileDataList);
            mFileListAdapter.setData(mListFile);
            showDataArea();
        } else {
            showNoDataArea();
        }

        mIsLoading = false;
        mActivityViewZipBinding.pullToRefresh.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        if (mZipPath != null) {
            File file = new File(mZipPath);
            if (file.exists()) {
                FileUtils.deleteRecursive(file);
            }
        }
        super.onDestroy();
    }

    private void showNoDataArea() {
        mActivityViewZipBinding.noDataErrorTv.setText(R.string.no_file_found);

        mActivityViewZipBinding.dataListArea.setVisibility(View.GONE);
        mActivityViewZipBinding.noDataErrorArea.setVisibility(View.VISIBLE);
        mActivityViewZipBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showDataArea() {
        mActivityViewZipBinding.dataListArea.setVisibility(View.VISIBLE);
        mActivityViewZipBinding.noDataErrorArea.setVisibility(View.GONE);
        mActivityViewZipBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showLoadingArea() {
        mActivityViewZipBinding.dataListArea.setVisibility(View.GONE);
        mActivityViewZipBinding.noDataErrorArea.setVisibility(View.GONE);
        mActivityViewZipBinding.loadingArea.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickItem(int position) {
        if (position >= 0 && position < mListFile.size()) {
            FileData fileData = mListFile.get(position);
            String filePath = fileData.getFilePath();
            if (fileData.getFileType().equals(DataConstants.FILE_TYPE_DIRECTORY)) {
                mCurrentFolder = fileData;
                reloadData(false);
                return;
            }
            if (filePath == null) {
                filePath = RealPathUtil.getInstance().getRealPath(this, mListFile.get(position).getFileUri());
            }

            if (filePath.toLowerCase().endsWith(".png")
                    || filePath.toLowerCase().endsWith(".jpg")
                    || filePath.toLowerCase().endsWith(".jpeg")) {
                FileUtils.openFile(this, filePath, "");
                return;
            }
            if (FileUtils.getFileType(new File(filePath)).length() == 0) {
                ToastUtils.showMessageShort(this, getString(R.string.file_not_supported));
                return;
            }

            String finalFilePath = filePath;
            showViewFileAdsBeforeAction(() -> {
                openDocumentFile(finalFilePath);
                mFileListAdapter.setCurrentItem(position);
            });
        }
    }

    @Override
    public void onMainFunctionItem(int position) {

    }

    @Override
    public void onOptionItem(int position) {

    }
}
