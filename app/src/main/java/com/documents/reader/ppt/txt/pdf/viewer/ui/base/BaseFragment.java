package com.documents.reader.ppt.txt.pdf.viewer.ui.base;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.FileInfoDialog;
import com.documents.reader.ppt.txt.pdf.viewer.utils.SnackBarUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.pdf.PdfUtils;

import java.io.File;

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseViewModel> extends Fragment {

    protected BaseBindingActivity mActivity;
    private View mRootView;
    private T mViewDataBinding;
    private V mViewModel;

    protected boolean mIsRequestFullPermission = false;
    protected int mRequestFullPermissionCode = -1;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * do something need
     */
    public abstract
    void reloadEasyChangeData();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseBindingActivity) {
            BaseBindingActivity activity = (BaseBindingActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = getViewModel();
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mRootView = mViewDataBinding.getRoot();
        return mRootView;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.executePendingBindings();
    }

    public BaseBindingActivity getBaseActivity() {
        return mActivity;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }


    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity() != null) {
            return getActivity().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    public void requestReadStoragePermissionsSafely(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
            intent.setData(uri);
            mIsRequestFullPermission = true;
            mRequestFullPermissionCode = requestCode;

            mActivity.startActivity(intent);
        }
    }

    protected void shareDocumentFile(String filePath) {
        FileUtils.shareFile(mActivity, new File(filePath));
    }

    protected void uploadDocumentFile(String filePath) {
        FileUtils.uploadFile(mActivity, new File(filePath));
    }

    protected void infoDocumentFile(String filePath) {
        FileInfoDialog fileInfoDialog = new FileInfoDialog(mActivity, () -> openDocumentFile(filePath), filePath);
        fileInfoDialog.show();
    }


    protected void printDocumentFile(String filePath) {
        if (PdfUtils.isPDFEncrypted(filePath)) {
            SnackBarUtils.getSnackbar(mActivity, mActivity.getString(R.string.view_pdf_can_not_print_protected_file)).show();
            return;
        }
        FileUtils.printFile(mActivity, new File(filePath));
    }

    protected void openDocumentFile(String filePath) {
        if (filePath == null) {
            return;
        }

        String fileType = FileUtils.getFileType(new File(filePath));
        if (fileType.equals(DataConstants.FILE_TYPE_PDF)) {
            mActivity.gotoPdfFileViewActivity(filePath);
        } else if (fileType.equals(DataConstants.FILE_TYPE_RAR) || fileType.equals(DataConstants.FILE_TYPE_ZIP)) {
            mActivity.gotoZipFileViewActivity(filePath);
        } else {
            mActivity.gotoDocumentFileViewActivity(filePath);
        }
    }

}