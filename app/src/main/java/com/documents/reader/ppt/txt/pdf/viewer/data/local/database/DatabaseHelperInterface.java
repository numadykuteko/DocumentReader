package com.documents.reader.ppt.txt.pdf.viewer.data.local.database;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;

import java.util.List;

import io.reactivex.Observable;

public interface DatabaseHelperInterface {
    Observable<Boolean> saveRecent(RecentData recentData);
    Observable<List<RecentData>> getListRecent();
    Observable<Boolean> clearRecent(String filePath);
    Observable<RecentData> getRecentByPath(String filePath);
}
