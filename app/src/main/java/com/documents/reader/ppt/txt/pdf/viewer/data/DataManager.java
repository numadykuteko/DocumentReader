package com.documents.reader.ppt.txt.pdf.viewer.data;

import android.content.Context;

import com.documents.reader.ppt.txt.pdf.viewer.data.local.database.DatabaseHelper;
import com.documents.reader.ppt.txt.pdf.viewer.data.local.preferences.PreferencesHelper;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.ViewPdfOption;
import com.documents.reader.ppt.txt.pdf.viewer.data.remote.ApiHelper;

import java.util.List;

import io.reactivex.Observable;

public class DataManager implements DataManagerInterface {
    private static DataManager mInstance;

    private final ApiHelper mApiHelper;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;

    private DataManager(Context context) {
        mDatabaseHelper = DatabaseHelper.getInstance(context);
        mPreferencesHelper = PreferencesHelper.getInstance(context);
        mApiHelper = ApiHelper.getInstance();
    }

    public static DataManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DataManager(context);
        }
        return mInstance;
    }

    @Override
    public boolean isOpenBefore() {
        return mPreferencesHelper.getOpenBefore() != 0;
    }

    @Override
    public void setOpenBefore() {
        mPreferencesHelper.setOpenBefore(1);
    }

    @Override
    public boolean checkRatingUsDone() {
        return mPreferencesHelper.getRatingUs() != 0;
    }

    @Override
    public void setRatingUsDone() {
        mPreferencesHelper.setRatingUs(1);
    }

    @Override
    public void setShowGuideConvertDone() {
        mPreferencesHelper.setShowGuideConvert(1);
    }

    @Override
    public boolean getShowGuideConvert() {
        return mPreferencesHelper.getShowGuideConvert() != 0;
    }

    @Override
    public void setShowGuideSelectMultiDone() {
        mPreferencesHelper.setShowGuideSelectMulti(1);
    }

    @Override
    public boolean getShowGuideSelectMulti() {
        return mPreferencesHelper.getShowGuideSelectMulti() == 0;
    }

    @Override
    public int getBackTime() {
        return mPreferencesHelper.getBackTime();
    }

    @Override
    public void increaseBackTime() {
        int time = mPreferencesHelper.getBackTime() + 1;
        mPreferencesHelper.setBackTime(time);
    }

    @Override
    public String getAppLocale() {
        return mPreferencesHelper.getAppLocale();
    }

    @Override
    public void setAppLocale(String locale) {
        mPreferencesHelper.setAppLocale(locale);
    }

    @Override
    public ViewPdfOption getViewPDFOptions() {
        return mPreferencesHelper.getViewPDFOptions();
    }

    @Override
    public void saveViewPDFOptions(ViewPdfOption viewPdfOption) {
        mPreferencesHelper.saveViewPDFOptions(viewPdfOption);
    }

    @Override
    public void setTheme(int theme) {
        mPreferencesHelper.setTheme(theme);
    }

    @Override
    public int getTheme() {
        return mPreferencesHelper.getTheme();
    }

    @Override
    public int getOpenFromOtherAppTime() {
        return mPreferencesHelper.getOpenFromOtherAppTime();
    }

    @Override
    public void setOpenFromOtherAppTime(int time) {
        mPreferencesHelper.setOpenFromOtherAppTime(time);
    }

    @Override
    public Observable<Boolean> saveRecent(RecentData recentData) {
        return mDatabaseHelper.saveRecent(recentData);
    }

    @Override
    public Observable<Boolean> saveRecent(String filePath, String type) {
        RecentData recentData = new RecentData();
        recentData.setFilePath(filePath);
        recentData.setActionType(type);
        return mDatabaseHelper.saveRecent(recentData);
    }

    @Override
    public Observable<Boolean> clearRecent(String filePath) {
        return mDatabaseHelper.clearRecent(filePath);
    }

    @Override
    public Observable<RecentData> getRecentByPath(String filePath) {
        return mDatabaseHelper.getRecentByPath(filePath);
    }

    @Override
    public Observable<List<RecentData>> getListRecent() {
        return mDatabaseHelper.getListRecent();
    }

}
