package com.documents.reader.ppt.txt.pdf.viewer.ui.recent;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class RecentViewModel extends BaseViewModel<RecentNavigator> {

    private ArrayList<RecentData> mListFile = new ArrayList<>();
    private MutableLiveData<List<RecentData>> mListFileLiveData = new MutableLiveData<>();

    public RecentViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<List<RecentData>> getListFileLiveData() {
        return mListFileLiveData;
    }

    public void getFileList(String filterType) {
        getCompositeDisposable().add(getDataManager()
                .getListRecent()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if (response != null && response.size() > 0) {
                        List<RecentData> output = new ArrayList<>();
                        if (!filterType.equals(DataConstants.FILE_TYPE_ALL_DOCUMENT)) {
                            for (RecentData recentData : response) {
                                if (recentData.getFilePath().toLowerCase().contains("." + filterType) && FileUtils.checkFileExist(recentData.getFilePath())) {
                                    output.add(recentData);
                                }
                            }
                        } else {
                            output = new ArrayList<>(response);
                            List<RecentData> needToRemove = new ArrayList<>();

                            for (RecentData recentData : response) {
                                if (!FileUtils.checkFileExist(recentData.getFilePath())) {
                                    needToRemove.add(recentData);
                                }
                            }

                            output.removeAll(needToRemove);
                            if (needToRemove.size() > 0) {
                                for (RecentData recentData : needToRemove) {
                                    clearRecent(recentData.getFilePath());
                                }
                            }
                        }

                        mListFileLiveData.postValue(output);
                    } else {
                        mListFileLiveData.postValue(new ArrayList<>());
                    }
                }, throwable -> {
                    mListFileLiveData.postValue(new ArrayList<>());
                })
        );
    }
}
