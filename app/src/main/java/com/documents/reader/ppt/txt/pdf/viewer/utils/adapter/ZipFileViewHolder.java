package com.documents.reader.ppt.txt.pdf.viewer.utils.adapter;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.listener.OnFileItemWithOptionClickListener;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileInfoUtils;

import java.io.File;

public class ZipFileViewHolder extends RecyclerView.ViewHolder {
    private ConstraintLayout mContentView;
    private ImageView mImageView;
    private TextView mNameView;
    private TextView mDateTextView;
    private ImageView mMoreView;

    public ZipFileViewHolder(@NonNull View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        mContentView = itemView.findViewById(R.id.item_content_view);
        mImageView = itemView.findViewById(R.id.item_image_view);
        mNameView = itemView.findViewById(R.id.item_name_view);
        mDateTextView = itemView.findViewById(R.id.item_date_text_view);
        mMoreView = itemView.findViewById(R.id.item_more_view);
    }

    @SuppressLint({"StaticFieldLeak", "UseCompatLoadingForDrawables", "SetTextI18n"})
    public void bindView(int position, FileData fileData, int currentItem, OnFileItemWithOptionClickListener listener) {

        if (fileData.getFileType().equals(DataConstants.FILE_TYPE_DIRECTORY)) {
            if (fileData.getDisplayName().equals(DataConstants.ALL_DOCUMENTS) && fileData.getFilePath().equals(Environment.getExternalStorageDirectory().getAbsolutePath())) {
                mNameView.setText(itemView.getContext().getString(R.string.all_documents));
            } else {
                mNameView.setText(fileData.getDisplayName());
            }
            mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_folder));

            mMoreView.setVisibility(View.GONE);
            mContentView.setLongClickable(false);
            mDateTextView.setText(itemView.getContext().getString(R.string.format_directory_info, fileData.getNumberChild()));
        } else {
            switch (fileData.getFileType()) {
                case DataConstants.FILE_TYPE_PDF:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_pdf));
                    break;
                case DataConstants.FILE_TYPE_EXCEL:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_excel));
                    break;
                case DataConstants.FILE_TYPE_WORD:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_word));
                    break;
                case DataConstants.FILE_TYPE_TXT:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_txt));
                    break;
                case DataConstants.FILE_TYPE_PPT:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_ppt));
                    break;
                case DataConstants.FILE_TYPE_CSV:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_csv));
                    break;
                case DataConstants.FILE_TYPE_RTF:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_rtf));
                    break;
                case DataConstants.FILE_TYPE_RAR:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_rar));
                    break;
                case DataConstants.FILE_TYPE_ZIP:
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_zip));
                    break;
                case "":
                    mImageView.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_no_document));
                    break;
            }

            if (fileData.getFilePath() != null) {
                mNameView.setText(fileData.getDisplayName());
            }
            File file = new File(fileData.getFilePath());

            if (fileData.getTimeAdded() > 0) {
                String text = itemView.getContext().getString(R.string.full_detail_file,
                        FileInfoUtils.getFormattedSize(file),
                        FileInfoUtils.getFormattedDate(file));
                mDateTextView.setText(text);
            } else {
                mDateTextView.setText(FileInfoUtils.getFormattedSize(file));
            }

            mMoreView.setVisibility(View.GONE);
            mMoreView.setOnClickListener(v -> {
            });
        }

        mContentView.setOnClickListener(v -> {
            listener.onClickItem(position);
        });
    }
}
