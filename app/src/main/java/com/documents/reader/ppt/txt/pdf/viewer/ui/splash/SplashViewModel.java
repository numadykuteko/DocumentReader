package com.documents.reader.ppt.txt.pdf.viewer.ui.splash;

import android.app.Application;

import androidx.annotation.NonNull;

import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {
    private static final String TAG = "SplashViewModel";

    public SplashViewModel(@NonNull Application application) {
        super(application);
    }
}
