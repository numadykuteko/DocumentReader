package com.documents.reader.ppt.txt.pdf.viewer.ui.browser;

import android.content.Context;
import android.os.AsyncTask;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.ui.search.SearchFileListener;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.util.ArrayList;

public class BrowserFileAsyncTask extends AsyncTask<Object, Object, Object> {
    private FileData mCurrentPath;
    private final Context mContext;
    private SearchFileListener mListener;
    private boolean mIsScanAll = false;

    public BrowserFileAsyncTask(Context context, SearchFileListener listener, FileData currentPath) {
        mContext = context;
        mListener = listener;
        mCurrentPath = currentPath;
    }

    public void setIsScanAll(boolean isScanAll) {
        this.mIsScanAll = isScanAll;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            ArrayList<FileData> allData = FileUtils.getFileListOfDirectory(mCurrentPath, mIsScanAll);

            if (!isCancelled() && mListener != null) {
                mListener.loadDone(allData);
            }
        } catch (Exception e) {
            mListener.loadDone(new ArrayList<>());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
