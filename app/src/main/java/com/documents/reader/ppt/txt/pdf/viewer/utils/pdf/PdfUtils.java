package com.documents.reader.ppt.txt.pdf.viewer.utils.pdf;

import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.pdf.PdfReader;

import java.io.IOException;

public class PdfUtils {
    public static boolean isPDFEncrypted(String path) {
        boolean isEncrypted;
        PdfReader pdfReader = null;
        try {
            pdfReader = new PdfReader(path);
            isEncrypted = pdfReader.isEncrypted();
        } catch (BadPasswordException e) {
            isEncrypted = true;
        } catch (OutOfMemoryError e) {
            isEncrypted = false;
        } catch (IOException e) {
            isEncrypted = false;
        } finally {
            if (pdfReader != null) pdfReader.close();
        }
        return isEncrypted;
    }

    public static boolean isPasswordValid(String filePath, byte[] password) {
        try {
            PdfReader pdfReader = new PdfReader(filePath, password);
            return true;
        } catch (BadPasswordException e) {
            return  false;
        } catch (OutOfMemoryError e) {
            return  false;
        } catch (IOException e) {
            return true;
        }
    }
}
