package com.documents.reader.ppt.txt.pdf.viewer.ui.folder;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class FolderViewModel extends BaseViewModel<FolderNavigator> {
    private FileData mCurrentPath;
    private FolderFileAsyncTask mAsyncTask;
    private int mSort;
    private String mType;

    private List<FileData> mListFile;
    private MutableLiveData<List<FileData>> mListFileLiveData = new MutableLiveData<>();
    public MutableLiveData<List<FileData>> getListFileLiveData() {
        return mListFileLiveData;
    }

    public FolderViewModel(@NonNull Application application) {
        super(application);
    }

    public void setCurrentFolder(FileData currentPath, int sort, String type) {
        this.mCurrentPath = currentPath;
        this.mSort = sort;
        this.mType = type;
    }

    public void getFileList() {
        mAsyncTask = new FolderFileAsyncTask(getApplication(), result -> {
            mListFile = new ArrayList<>(result);

            mListFileLiveData.postValue(mListFile);
            }, mCurrentPath, mSort, mType);
        mAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
