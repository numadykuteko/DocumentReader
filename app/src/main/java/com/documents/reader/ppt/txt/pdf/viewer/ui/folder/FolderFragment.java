package com.documents.reader.ppt.txt.pdf.viewer.ui.folder;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ads.control.Admod;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.FragmentFolderBinding;
import com.documents.reader.ppt.txt.pdf.viewer.listener.OnFileItemWithOptionClickListener;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseFragment;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.ConfirmDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.FileOptionDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.RenameFileDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.main.MainActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ColorUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.CommonUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DialogFactory;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.SnackBarUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.adapter.BrowserAdapter;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.RealPathUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class FolderFragment extends BaseFragment<FragmentFolderBinding, FolderViewModel> implements FolderNavigator, OnFileItemWithOptionClickListener {

    private FolderViewModel mFolderViewModel;
    private FragmentFolderBinding mFragmentFolderBinding;
    private boolean mIsLoading;
    private List<FileData> mListFile = new ArrayList<>();
    private BrowserAdapter mFileListAdapter;

    private SweetAlertDialog mRequestPermissionDialog;
    private final int REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE = 1;

    private FileOptionDialog fileOptionDialog;

    private FileData mCurrentFolder;

    private String mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;
    private boolean mIsFabOpen = false;
    private int mCurrentSortBy = FileUtils.SORT_BY_DATE_DESC;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_folder;
    }

    @Override
    public void reloadEasyChangeData() {

    }

    @SuppressLint("SetTextI18n")
    public void reloadData(boolean isForceReload) {
        if (mActivity != null && mActivity.notHaveStoragePermission()) {
            mFragmentFolderBinding.pullToRefresh.setRefreshing(false);

            showPermissionIssueArea();
            mIsLoading = false;
            return;
        }

        if (mIsLoading) return;

        mIsLoading = true;

        String nameFolder = null;
        if (mCurrentFolder != null) {
            File dir = Environment.getExternalStorageDirectory();
            if (mCurrentFolder.getFilePath().equals(dir.getAbsolutePath())) {
                nameFolder = "root";
            } else {
                nameFolder = FileUtils.getFileName(mCurrentFolder.getFilePath());
            }

            if (mActivity != null && mActivity instanceof MainActivity) {
                ((MainActivity) mActivity).setVisibleBtnToolbar(View.VISIBLE);
            }

            mFragmentFolderBinding.fabMenuSort.sortLayout.setVisibility(View.VISIBLE);
        } else {
            if (mActivity != null && mActivity instanceof MainActivity) {
                ((MainActivity) mActivity).setVisibleBtnToolbar(View.GONE);
            }
            mFragmentFolderBinding.fabMenuSort.sortLayout.setVisibility(View.GONE);
        }
        if (mActivity != null && mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).setToolbarName(nameFolder);
        }

        if (mListFile == null || mListFile.size() == 0 || isForceReload) {
            showLoadingArea();
        }

        mFolderViewModel.setCurrentFolder(mCurrentFolder, mCurrentSortBy, mFilterType);
        mFolderViewModel.getFileList();
    }

    @Override
    public FolderViewModel getViewModel() {
        mFolderViewModel = ViewModelProviders.of(this).get(FolderViewModel.class);
        return mFolderViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mFolderViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentFolderBinding = getViewDataBinding();

        setForClick();
        initView();
        setForLiveData();

        startRequestPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE:
                if ((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    mRequestPermissionDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    mRequestPermissionDialog.setTitleText(getString(R.string.thankyou_text));
                    mRequestPermissionDialog.setContentText(getString(R.string.get_file_now));
                    mRequestPermissionDialog.showCancelButton(false);
                    mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                    mRequestPermissionDialog.setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismiss();
                        reloadData(true);
                    });
                } else {
                    mRequestPermissionDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    mRequestPermissionDialog.setTitleText(getString(R.string.title_need_permission_fail));
                    mRequestPermissionDialog.setContentText(getString(R.string.couldnt_get_file_now));
                    mRequestPermissionDialog.showCancelButton(false);
                    mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                    mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        if (mIsRequestFullPermission) {
            mIsRequestFullPermission = false;

            if (!mActivity.notHaveStoragePermission()) {
                mRequestPermissionDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                mRequestPermissionDialog.setTitleText(getString(R.string.thankyou_text));
                mRequestPermissionDialog.setContentText(getString(R.string.get_file_now));
                mRequestPermissionDialog.showCancelButton(false);
                mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);

                reloadData(true);
            } else {
                mRequestPermissionDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                mRequestPermissionDialog.setTitleText(getString(R.string.title_need_permission_fail));
                mRequestPermissionDialog.setContentText(getString(R.string.couldnt_get_file_now));
                mRequestPermissionDialog.showCancelButton(false);
                mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
            }
        }

        super.onResume();
    }


    public static FolderFragment newInstance() {
        FolderFragment folderFragment = new FolderFragment();

        Bundle args = new Bundle();
        folderFragment.setArguments(args);
        folderFragment.setRetainInstance(true);

        return folderFragment;
    }

    private void initView() {
        Admod.getInstance().loadBanner(mActivity, BuildConfig.banner_id);

        mFragmentFolderBinding.pullToRefresh.setOnRefreshListener(() -> {
            reloadData(false);
        });

        mFileListAdapter = new BrowserAdapter(this);
        mFragmentFolderBinding.dataListArea.setLayoutManager(new LinearLayoutManager(mActivity));
        mFragmentFolderBinding.dataListArea.setAdapter(mFileListAdapter);

        mCurrentFolder = null;

        mIsFabOpen = false;
        setForFabSort();
    }

    public boolean isFabOpen() {
        return mIsFabOpen;
    }

    public void closeFab() {
        if (mIsFabOpen) {
            mIsFabOpen = false;
            updateFabStatus();
        }
    }

    private void setForFabSort() {
        mFragmentFolderBinding.fabMenuSort.fabViewSort.setOnClickListener(view -> {
            if (mIsFabOpen) {
                mIsFabOpen = false;
                updateFabStatus();
            } else {
                mIsFabOpen = true;
                updateFabStatus();
            }
        });

        mFragmentFolderBinding.fabMenuSort.textSortDateAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_ASC);
        });
        mFragmentFolderBinding.fabMenuSort.fabSortDateAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_ASC);
        });

        mFragmentFolderBinding.fabMenuSort.textSortDateDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_DESC);
        });
        mFragmentFolderBinding.fabMenuSort.fabSortDateDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_DESC);
        });

        mFragmentFolderBinding.fabMenuSort.textSortSizeAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_ASC);
        });
        mFragmentFolderBinding.fabMenuSort.fabSortSizeAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_ASC);
        });

        mFragmentFolderBinding.fabMenuSort.textSortSizeDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_DESC);
        });
        mFragmentFolderBinding.fabMenuSort.fabSortSizeDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_DESC);
        });

        mFragmentFolderBinding.fabMenuSort.textSortNameAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_ASC);
        });
        mFragmentFolderBinding.fabMenuSort.fabSortNameAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_ASC);
        });

        mFragmentFolderBinding.fabMenuSort.textSortNameDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_DESC);
        });
        mFragmentFolderBinding.fabMenuSort.fabSortNameDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_DESC);
        });
    }

    private void setForSortValue(int newValue) {
        if (mIsLoading) {
            ToastUtils.showMessageShort(mActivity, getString(R.string.sort_not_available));
            return;
        }
        mCurrentSortBy = newValue;
        reloadData(true);

        mIsFabOpen = false;
        updateFabStatus();
    }

    private void updateFabStatus() {
        mFragmentFolderBinding.fabMenuSort.fabSortDateAsc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.fabSortDateDesc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.fabSortNameAsc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.fabSortNameDesc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.fabSortSizeAsc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.fabSortSizeDesc.setClickable(mIsFabOpen);

        mFragmentFolderBinding.fabMenuSort.textSortDateAsc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.textSortDateDesc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.textSortNameAsc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.textSortNameDesc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.textSortSizeAsc.setClickable(mIsFabOpen);
        mFragmentFolderBinding.fabMenuSort.textSortSizeDesc.setClickable(mIsFabOpen);

        Animation mFabCloseAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.fab_close);
        Animation mFabOpenAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.fab_open);
        Animation mTextOpenAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.text_open);
        Animation mTextCloseAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.text_close);
        Animation fabAnimation, textAnimation;

        if (mIsFabOpen) {
            fabAnimation = mFabOpenAnimation;
            textAnimation = mTextOpenAnimation;
            mFragmentFolderBinding.fabMenuSort.contentFab.setBackgroundColor(ColorUtils.getColorFromResource(mActivity, R.color.white_semi_transparent));
            mFragmentFolderBinding.fabMenuSort.contentFab.setClickable(true);
            mFragmentFolderBinding.fabMenuSort.contentFab.setFocusable(true);
            mFragmentFolderBinding.fabMenuSort.contentFab.setVisibility(View.VISIBLE);
            mFragmentFolderBinding.fabMenuSort.contentFab.setOnClickListener(v -> {
                mIsFabOpen = false;
                updateFabStatus();
            });

            int whiteColor = ColorUtils.getColorFromResource(mActivity, R.color.white_totally);
            int selectedColor = ColorUtils.getColorFromResource(mActivity, R.color.light_blue);

            mFragmentFolderBinding.fabMenuSort.textSortDateAsc.setTextColor(whiteColor);
            mFragmentFolderBinding.fabMenuSort.textSortDateDesc.setTextColor(whiteColor);
            mFragmentFolderBinding.fabMenuSort.textSortSizeAsc.setTextColor(whiteColor);
            mFragmentFolderBinding.fabMenuSort.textSortSizeDesc.setTextColor(whiteColor);
            mFragmentFolderBinding.fabMenuSort.textSortNameAsc.setTextColor(whiteColor);
            mFragmentFolderBinding.fabMenuSort.textSortNameDesc.setTextColor(whiteColor);

            switch (mCurrentSortBy) {
                case FileUtils.SORT_BY_DATE_ASC:
                    mFragmentFolderBinding.fabMenuSort.textSortDateAsc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_DATE_DESC:
                    mFragmentFolderBinding.fabMenuSort.textSortDateDesc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_SIZE_ASC:
                    mFragmentFolderBinding.fabMenuSort.textSortSizeAsc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_SIZE_DESC:
                    mFragmentFolderBinding.fabMenuSort.textSortSizeDesc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_NAME_ASC:
                    mFragmentFolderBinding.fabMenuSort.textSortNameAsc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_NAME_DESC:
                    mFragmentFolderBinding.fabMenuSort.textSortNameDesc.setTextColor(selectedColor);
                    break;
            }

        } else {
            fabAnimation = mFabCloseAnimation;
            textAnimation = mTextCloseAnimation;

            mFragmentFolderBinding.fabMenuSort.contentFab.setBackgroundColor(ColorUtils.getColorFromResource(mActivity, R.color.transparent));
            mFragmentFolderBinding.fabMenuSort.contentFab.setClickable(false);
            mFragmentFolderBinding.fabMenuSort.contentFab.setFocusable(false);
            mFragmentFolderBinding.fabMenuSort.contentFab.setVisibility(View.GONE);
            mFragmentFolderBinding.fabMenuSort.contentFab.setOnClickListener(v -> {});
        }

        mFragmentFolderBinding.fabMenuSort.fabSortDateAsc.startAnimation(fabAnimation);
        mFragmentFolderBinding.fabMenuSort.fabSortDateDesc.startAnimation(fabAnimation);
        mFragmentFolderBinding.fabMenuSort.fabSortNameAsc.startAnimation(fabAnimation);
        mFragmentFolderBinding.fabMenuSort.fabSortNameDesc.startAnimation(fabAnimation);
        mFragmentFolderBinding.fabMenuSort.fabSortSizeAsc.startAnimation(fabAnimation);
        mFragmentFolderBinding.fabMenuSort.fabSortSizeDesc.startAnimation(fabAnimation);

        mFragmentFolderBinding.fabMenuSort.textSortDateAsc.startAnimation(textAnimation);
        mFragmentFolderBinding.fabMenuSort.textSortDateDesc.startAnimation(textAnimation);
        mFragmentFolderBinding.fabMenuSort.textSortNameAsc.startAnimation(textAnimation);
        mFragmentFolderBinding.fabMenuSort.textSortNameDesc.startAnimation(textAnimation);
        mFragmentFolderBinding.fabMenuSort.textSortSizeAsc.startAnimation(textAnimation);
        mFragmentFolderBinding.fabMenuSort.textSortSizeDesc.startAnimation(textAnimation);
    }

    public boolean isCurrentFolderRoot() {
        return mCurrentFolder == null;
    }

    public void onBackPress() {
        try {
            if (mCurrentFolder != null) {
                if (mIsLoading) return;
                mCurrentFolder = null;
                mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;
                reloadData(false);
            }

        } catch (Exception ignored) {
        }
    }

    private void setForClick() {
        if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).setClickFilterItem(view -> showFilterOptions(view));
        }
    }

    @SuppressLint("NonConstantResourceId")
    private void showFilterOptions(View view) {
        if (mIsFabOpen) {
            mIsFabOpen = false;
            updateFabStatus();
        }

        PopupMenu popup = new PopupMenu(mActivity, view);
        popup.inflate(R.menu.menu_filter_file);
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.all_type:
                    mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;
                    reloadData(true);
                    return false;
                case R.id.pdf_type:
                    mFilterType = DataConstants.FILE_TYPE_PDF;
                    reloadData(true);
                    return false;
                case R.id.word_type:
                    mFilterType = DataConstants.FILE_TYPE_WORD;
                    reloadData(true);
                    return false;
                case R.id.excel_type:
                    mFilterType = DataConstants.FILE_TYPE_EXCEL;
                    reloadData(true);
                    return false;
                case R.id.csv_type:
                    mFilterType = DataConstants.FILE_TYPE_CSV;
                    reloadData(true);
                    return false;
                case R.id.ppt_type:
                    mFilterType = DataConstants.FILE_TYPE_PPT;
                    reloadData(true);
                    return false;
                case R.id.txt_type:
                    mFilterType = DataConstants.FILE_TYPE_TXT;
                    reloadData(true);
                    return false;
                case R.id.rar_type:
                    mFilterType = DataConstants.FILE_TYPE_RAR;
                    reloadData(true);
                    return false;
                case R.id.zip_type:
                    mFilterType = DataConstants.FILE_TYPE_ZIP;
                    reloadData(true);
                    return false;
                default:
                    return false;
            }
        });

        CommonUtils.insertMenuItemIcons(mActivity, popup);
        popup.show();
    }

    private void setForLiveData() {
        mFolderViewModel.getListFileLiveData().observe(getViewLifecycleOwner(), this::updateData);
    }

    private void updateData(List<FileData> fileDataList) {
        if (fileDataList.size() > 0) {
            if (fileDataList.equals(mListFile)) {
                mIsLoading = false;
                mFragmentFolderBinding.pullToRefresh.setRefreshing(false);

                return;
            }

            mListFile = new ArrayList<>();
            mListFile.addAll(fileDataList);

            mFileListAdapter.setData(mListFile);

            showDataArea();
        } else {
            showNoDataArea();
        }

        mIsLoading = false;
        mFragmentFolderBinding.pullToRefresh.setRefreshing(false);
    }

    private void startRequestPermission() {
        if (mActivity != null && mActivity.notHaveStoragePermission()) {
            showPermissionIssueArea();
            mRequestPermissionDialog = DialogFactory.getDialogRequestSomething(mActivity, getString(R.string.title_need_permission), getString(R.string.need_permission_to_get_file));
            mRequestPermissionDialog.setConfirmClickListener(sweetAlertDialog -> {
                requestReadStoragePermissionsSafely(REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE);
            });
            mRequestPermissionDialog.setCancelClickListener(sweetAlertDialog -> {
                sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(getString(R.string.title_need_permission_fail));
                sweetAlertDialog.setContentText(getString(R.string.couldnt_get_file_now));
                sweetAlertDialog.setConfirmClickListener(Dialog::dismiss);
                sweetAlertDialog.showCancelButton(false);
                sweetAlertDialog.setConfirmText(getString(R.string.confirm_text));
            });
            mRequestPermissionDialog.show();
        } else {
            reloadData(true);
        }
    }

    private void showNoDataArea() {
        mFragmentFolderBinding.noDataErrorTv.setText(R.string.no_file_found);

        mFragmentFolderBinding.dataListArea.setVisibility(View.GONE);
        mFragmentFolderBinding.dataListArea.setVisibility(View.GONE);
        mFragmentFolderBinding.noDataErrorArea.setVisibility(View.VISIBLE);
        mFragmentFolderBinding.noPermissionArea.setVisibility(View.GONE);
        mFragmentFolderBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showPermissionIssueArea() {
        mFragmentFolderBinding.noPermissionArea.setOnClickListener(v -> {
            startRequestPermission();
        });
        mFragmentFolderBinding.dataListArea.setVisibility(View.GONE);
        mFragmentFolderBinding.noDataErrorArea.setVisibility(View.GONE);
        mFragmentFolderBinding.noPermissionArea.setVisibility(View.VISIBLE);
        mFragmentFolderBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showDataArea() {
        mFragmentFolderBinding.dataListArea.setVisibility(View.VISIBLE);
        mFragmentFolderBinding.noDataErrorArea.setVisibility(View.GONE);
        mFragmentFolderBinding.noPermissionArea.setVisibility(View.GONE);
        mFragmentFolderBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showLoadingArea() {
        mFragmentFolderBinding.dataListArea.setVisibility(View.GONE);
        mFragmentFolderBinding.noDataErrorArea.setVisibility(View.GONE);
        mFragmentFolderBinding.loadingArea.setVisibility(View.VISIBLE);
        mFragmentFolderBinding.noPermissionArea.setVisibility(View.GONE);
    }

    @Override
    public void onClickItem(int position) {
        if (position >= 0 && position < mListFile.size()) {
            FileData fileData = mListFile.get(position);
            String filePath = fileData.getFilePath();
            if (fileData.getFileType().equals(DataConstants.FILE_TYPE_DIRECTORY)) {
                if (position == 0 && fileData.getDisplayName().equals(DataConstants.ALL_DOCUMENTS)) {
                    if (mActivity != null && mActivity instanceof MainActivity) {
                        ((MainActivity) mActivity).gotoFileView();
                    }

                    return;
                }
                mCurrentFolder = fileData;
                reloadData(false);
                return;
            }
            if (filePath == null) {
                filePath = RealPathUtil.getInstance().getRealPath(mActivity, mListFile.get(position).getFileUri());
            }

            String finalFilePath = filePath;
            if (mActivity != null) {
                mActivity.showViewFileAdsBeforeAction(() -> {
                    openDocumentFile(finalFilePath);
                    mFileListAdapter.setCurrentItem(position);
                });
            }
        }

    }

    @Override
    public void onMainFunctionItem(int position) {
        onShowOption(position);
    }

    @Override
    public void onOptionItem(int position) {

    }

    private void onShowOption(int position) {
        FileData fileData = mListFile.get(position);

        hideOptionDialog();
        fileOptionDialog = new FileOptionDialog(fileData.getDisplayName(), fileData.getTimeAdded(), position, new FileOptionDialog.FileOptionListener() {
            @Override
            public void openFile(int position) {
                openDocumentFile(position);
            }

            @Override
            public void shareFile(int position) {
                shareDocumentFile(fileData.getFilePath());
            }

            @Override
            public void printFile(int position) {
                printDocumentFile(fileData.getFilePath());
            }

            @Override
            public void infoFile(int position) {
                infoDocumentFile(fileData.getFilePath());
            }

            @Override
            public void renameFile(int position) {
                renameDocumentFile(position);
            }

            @Override
            public void deleteFile(int position) {
                deleteDocumentFile(position);
            }
        });
        fileOptionDialog.show(getChildFragmentManager(), fileOptionDialog.getTag());
        CommonUtils.hideKeyboard(mActivity);
    }

    private void hideOptionDialog() {
        if (fileOptionDialog != null && fileOptionDialog.isVisible()) {
            fileOptionDialog.dismiss();
        }
    }

    private void openDocumentFile(int position) {
        onClickItem(position);
    }

    private void renameDocumentFile(int position) {
        FirebaseUtils.sendEventFunctionUsed(mActivity, FirebaseUtils.EVENT_NAME_LIST_FILE, "Rename file");

        FileData fileData = mListFile.get(position);
        String displayName;
        String extension;
        try {
            displayName = fileData.getDisplayName().substring(0, fileData.getDisplayName().lastIndexOf("."));
            extension = fileData.getDisplayName().substring(fileData.getDisplayName().lastIndexOf("."));
        } catch (Exception e) {
            return;
        }

        RenameFileDialog renameFileDialog = new RenameFileDialog(mActivity, displayName, new RenameFileDialog.RenameFileListener() {
            @Override
            public void onSubmitName(String name) {
                String newName = name + extension;
                int result = FileUtils.renameFile(fileData, newName);

                if (result == -2 || result == 0) {
                    ToastUtils.showMessageShort(mActivity, getString(R.string.can_not_edit_video_name));
                } else if (result == -1) {
                    SnackBarUtils.getSnackbar(mActivity, getString(R.string.duplicate_video_name) + ": " + name).show();
                } else {
                    SnackBarUtils.getSnackbar(mActivity, getString(R.string.rename_file_success)).show();
                    String oldFilePath = fileData.getFilePath();

                    fileData.setFilePath(fileData.getFilePath().replace(fileData.getDisplayName(), newName));
                    fileData.setDisplayName(newName);
                    mListFile.set(position, fileData);
                    mFileListAdapter.updateData(position, fileData);

                    mFolderViewModel.updateSavedData(oldFilePath, fileData.getFilePath());
                }
            }

            @Override
            public void onCancel() {

            }
        });

        renameFileDialog.show();
    }

    private void deleteDocumentFile(int position) {
        FirebaseUtils.sendEventFunctionUsed(mActivity, FirebaseUtils.EVENT_NAME_LIST_FILE, "Delete file");

        FileData fileData = mListFile.get(position);

        ConfirmDialog confirmDialog = new ConfirmDialog(mActivity, mActivity.getString(R.string.confirm_delete_file_title), mActivity.getString(R.string.confirm_delete_file_message), new ConfirmDialog.ConfirmListener() {
            @Override
            public void onSubmit() {
                if (mActivity != null && !mActivity.notHaveStoragePermission()) {
                    FileUtils.deleteFileOnExist(fileData.getFilePath());
                    mFolderViewModel.clearSavedData(fileData.getFilePath());

                    if (position >= 0 && position < mListFile.size()) {
                        mListFile.remove(position);
                        if (position == 0 && mListFile.size() > 1) {
                            Collections.swap(mListFile, 0, 1);
                        }
                    }

                    mFileListAdapter.clearData(position);
                    if (mListFile.size() <= 1) {
                        showNoDataArea();
                    }
                    SnackBarUtils.getSnackbar(mActivity, mActivity.getString(R.string.delete_success_text)).show();
                    hideOptionDialog();
                }
            }

            @Override
            public void onCancel() {

            }
        });
        confirmDialog.show();
    }
}
