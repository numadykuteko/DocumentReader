package com.documents.reader.ppt.txt.pdf.viewer.ui.viewzip;

public interface ExactFileListener {
    public void loadDone(String result);
}
