package com.documents.reader.ppt.txt.pdf.viewer.utils.ads;

import android.content.Context;

public class RateShowCountManager {
    private static final String LOG = "RateShowCountManager";

    private static RateShowCountManager mInstance;
    private double mCountForClickItem;

    private RateShowCountManager() {
        mCountForClickItem = 1;
    }

    public static RateShowCountManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RateShowCountManager();
        }

        return mInstance;
    }


    public boolean checkShowRate() {
        return mCountForClickItem >= 3;
    }

    public void increaseCountForShowRate() {
        if (mCountForClickItem >= 3) {
            mCountForClickItem = 1;
        } else {
            mCountForClickItem ++;
        }
    }

}
