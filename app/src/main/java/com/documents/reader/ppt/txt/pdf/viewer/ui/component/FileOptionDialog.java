package com.documents.reader.ppt.txt.pdf.viewer.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DateTimeUtils;

public class FileOptionDialog extends BottomSheetDialogFragment {
    private FileOptionListener mListener;
    private String mNameFile;
    private int mPosition;
    private long mDate;

    public FileOptionDialog() {
        // Required empty public constructor
    }

    public FileOptionDialog(String nameFile, long date, int position, FileOptionListener listener) {
        this.mListener = listener;
        mNameFile = nameFile;
        mPosition = position;
        mDate = date;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.sheet_dialog_style);
    }

    private void setAutoExpanded() {
        if (getDialog() != null) {
            getDialog().setOnShowListener(dialog -> {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                View bottomSheetInternal = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                if (bottomSheetInternal != null) {
                    BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            });
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_file_option, container, false);

        TextView nameFile = v.findViewById(R.id.more_name);
        nameFile.setText(mNameFile);
        nameFile.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.openFile(mPosition);
            }
            dismiss();
        });
        
        ImageView imageFile = v.findViewById(R.id.more_icon);
        Context context = getContext();
        if (context != null) {
            if (mNameFile.toLowerCase().endsWith(DataConstants.PDF_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_pdf));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.DOC_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.DOCX_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_word));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.EXCEL_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.EXCEL_WORKBOOK_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_excel));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.PPT_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.PPTX_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.POT_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.PPTM_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.POTX_EXTENSION) ||
                    mNameFile.toLowerCase().endsWith(DataConstants.POTM_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_ppt));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.TEXT_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_txt));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.EXCEL_CSV_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_csv));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.RTF_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_rtf));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.ZIP_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_zip));
            } else if (mNameFile.toLowerCase().endsWith(DataConstants.RAR_EXTENSION)) {
                imageFile.setImageDrawable(context.getDrawable(R.drawable.ic_rar));
            }
        }

        TextView dateFile = v.findViewById(R.id.more_date);
        if (mDate != -1) {
            dateFile.setText(DateTimeUtils.fromTimeUnixToDateString(mDate));
        } else {
            dateFile.setText("N/A");
        }

        ConstraintLayout info = v.findViewById(R.id.more_layout_info);
        info.setOnClickListener(v1 -> {
            if (mListener != null) {
                mListener.infoFile(mPosition);
                FirebaseUtils.sendEventFunctionUsed(getContext(), FirebaseUtils.EVENT_NAME_LIST_FILE, "Info file");
            }
            dismiss();
        });

        ConstraintLayout print = v.findViewById(R.id.more_layout_print);
        if (mNameFile.toLowerCase().endsWith(DataConstants.PDF_EXTENSION)) {
            print.setVisibility(View.VISIBLE);

            print.setOnClickListener(v1 -> {
                if (mListener != null) {
                    mListener.printFile(mPosition);
                    FirebaseUtils.sendEventFunctionUsed(getContext(), FirebaseUtils.EVENT_NAME_LIST_FILE, "Print file");
                }
                dismiss();
            });
        } else {
            print.setVisibility(View.GONE);
        }

        ConstraintLayout openOption = v.findViewById(R.id.more_layout_open);
        openOption.setOnClickListener(v1 -> {
            if (mListener != null) {
                mListener.openFile(mPosition);
            }
            dismiss();
        });

        ConstraintLayout shareOption = v.findViewById(R.id.more_layout_share);
        shareOption.setOnClickListener(v1 -> {
            if (mListener != null) {
                mListener.shareFile(mPosition);
                FirebaseUtils.sendEventFunctionUsed(getContext(), FirebaseUtils.EVENT_NAME_LIST_FILE, "Share file");
            }
            dismiss();
        });

        ConstraintLayout renameOption = v.findViewById(R.id.more_layout_rename);
        renameOption.setOnClickListener(v1 -> {
            if (mListener != null) {
                mListener.renameFile(mPosition);
            }
            dismiss();
        });

        ConstraintLayout deleteOption = v.findViewById(R.id.more_layout_delete);
        deleteOption.setOnClickListener(v1 -> {
            if (mListener != null) {
                mListener.deleteFile(mPosition);
            }
            dismiss();
        });

        return v;
    }

    public interface FileOptionListener {
        void openFile(int position);
        void shareFile(int position);
        void printFile(int position);
        void infoFile(int position);
        void renameFile(int position);
        void deleteFile(int position);
    }
}
