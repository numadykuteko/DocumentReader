package com.documents.reader.ppt.txt.pdf.viewer.utils.file;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.SavedData;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.github.junrar.Junrar;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.os.ParcelFileDescriptor.MODE_READ_ONLY;
import static com.documents.reader.ppt.txt.pdf.viewer.utils.pdf.ImageToPdfConstants.AUTHORITY_APP;

public class FileUtils {
    public enum FileType {
        type_PDF,
        type_TXT,
        type_EXCEL,
        type_WORD,
        type_IMAGE,
        type_PPT
    }

    private static final List<String> orderList = Arrays.asList(MediaStore.Files.FileColumns.DATE_ADDED, MediaStore.Files.FileColumns.DISPLAY_NAME, MediaStore.Files.FileColumns.SIZE);
    public static final int SORT_BY_DATE_ASC = 0;
    public static final int SORT_BY_DATE_DESC = 1;
    public static final int SORT_BY_NAME_ASC = 2;
    public static final int SORT_BY_NAME_DESC = 3;
    public static final int SORT_BY_SIZE_ASC = 4;
    public static final int SORT_BY_SIZE_DESC = 5;
    public static final int SORT_BY_TYPE = 6;
    public static final int SORT_BY_TYPE_REVERT = 7;

    public static ArrayList<FileData> getExternalFileList(Context context, String fileType, int order) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = MediaStore.Files.getContentUri("external");

        String[] projection = {MediaStore.Files.FileColumns._ID, MediaStore.Files.FileColumns.DISPLAY_NAME, MediaStore.Files.FileColumns.DATE_MODIFIED, MediaStore.Files.FileColumns.SIZE};
        String selectionMimeType;
        String[] selectionArgsPdf;
        if (fileType.equals(DataConstants.FILE_TYPE_WORD)) {
            selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "= ? OR " + MediaStore.Files.FileColumns.MIME_TYPE + "= ?";
            selectionArgsPdf = new String[]{MimeTypeMap.getSingleton().getMimeTypeFromExtension("doc"), MimeTypeMap.getSingleton().getMimeTypeFromExtension("docx")};
        } else if (fileType.equals(DataConstants.FILE_TYPE_EXCEL)) {
            selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "= ? OR " + MediaStore.Files.FileColumns.MIME_TYPE + "= ?";
            selectionArgsPdf = new String[]{MimeTypeMap.getSingleton().getMimeTypeFromExtension("xls"), MimeTypeMap.getSingleton().getMimeTypeFromExtension("xlsx")};
        } else {
            selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=?";
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileType);
            selectionArgsPdf = new String[]{mimeType};
        }

        String orderBy;
        if (order == 1) {
            orderBy = orderList.get(order) + " ASC";
        } else {
            orderBy = orderList.get(order) + " DESC";
        }

        Cursor cursor = cr.query(uri, projection, selectionMimeType, selectionArgsPdf, orderBy);
        ArrayList<FileData> fileList = new ArrayList<>();
        if (cursor != null) {

            while (cursor.moveToNext()) {

                int columnIdIndex = cursor.getColumnIndex(projection[0]);
                int columnNameIndex = cursor.getColumnIndex(projection[1]);
                int columnDateIndex = cursor.getColumnIndex(projection[2]);
                int columnSizeIndex = cursor.getColumnIndex(projection[3]);

                long fileId = -1;
                try {
                    fileId = cursor.getLong(columnIdIndex);
                } catch (Exception e) {
                    continue;
                }

                Uri fileUri = Uri.parse(uri.toString() + "/" + fileId);

                String displayName = cursor.getString(columnNameIndex);
                if (displayName == null || displayName.length() == 0) {
                    displayName = "No name";
                }

                int dateAdded;
                try {
                    dateAdded = Integer.parseInt(cursor.getString(columnDateIndex));
                } catch (Exception e) {
                    dateAdded = -1;
                }

                int size = 0;
                try {
                    size = Integer.parseInt(cursor.getString(columnSizeIndex));
                } catch (Exception e) {
                    size = -1;
                }

                fileList.add(new FileData(displayName, null, fileUri, dateAdded, size, fileType));
            }
            cursor.close();
        }
        return fileList;
    }

    /**
     * Uri selectedFileUri = data.getData();
     * String selectedImagePath = FileUtils.getRealPathV3(selectedFileUri, context);
     */
    public static String getRealPathV3(Context context, Uri uri) {
        if (DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String[] split = DocumentsContract.getDocumentId(uri).split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {
                String uriId = DocumentsContract.getDocumentId(uri);
                if (uriId.startsWith("raw:/")) {
                    return uriId.replace("raw:/", "");
                } else if (uriId.startsWith("msf:")) {
                    return null;
                } else {
                    try {
                        long idLong = Long.parseLong(uriId);
                        final Uri downloadContentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), idLong);

                        return getDataColumn(context, downloadContentUri, null, null);
                    } catch (NumberFormatException e) {
                        return null;
                    }
                }
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {

                final int column_index = cursor.getColumnIndexOrThrow(column);
                return getIndexAsString(cursor, column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static String getIndexAsString(Cursor c, int i) {
        if (i == -1)
            return null;
        if (c.isNull(i)) {
            return null;
        }
        switch (c.getType(i)) {
            case Cursor.FIELD_TYPE_STRING:
                return c.getString(i);
            case Cursor.FIELD_TYPE_FLOAT: {
                return Double.toString(c.getDouble(i));
            }
            case Cursor.FIELD_TYPE_INTEGER: {
                return Long.toString(c.getLong(i));
            }
            default:
            case Cursor.FIELD_TYPE_NULL:
            case Cursor.FIELD_TYPE_BLOB:
                throw new IllegalStateException("data null");
        }
    }

    public static ArrayList<FileData> getAllExternalFileList(Context context, String fileType, int order) {
        DirectoryUtils directoryUtils = new DirectoryUtils(context);
        ArrayList<String> fileList = new ArrayList<>();

        switch (fileType) {
            case DataConstants.FILE_TYPE_ALL_DOCUMENT:
                fileList = directoryUtils.getAllDocumentsOnDevice();
                break;
            case DataConstants.FILE_TYPE_WORD:
                fileList = directoryUtils.getAllWordsOnDevice();
                break;
            case DataConstants.FILE_TYPE_TXT:
                fileList = directoryUtils.getAllTxtsOnDevice();
                break;
            case DataConstants.FILE_TYPE_PDF:
                fileList = directoryUtils.getAllPDFsOnDevice();
                break;
            case DataConstants.FILE_TYPE_EXCEL:
                fileList = directoryUtils.getAllExcelsOnDevice();
                break;
            case DataConstants.FILE_TYPE_CSV:
                fileList = directoryUtils.getAllCsvOnDevice();
                break;
            case DataConstants.FILE_TYPE_PPT:
                fileList = directoryUtils.getAllPptsOnDevice();
                break;
            case DataConstants.FILE_TYPE_RTF:
                fileList = directoryUtils.getAllRTFOnDevice();
                break;
            case DataConstants.FILE_TYPE_RAR:
                fileList = directoryUtils.getAllRarOnDevice();
                break;
            case DataConstants.FILE_TYPE_ZIP:
                fileList = directoryUtils.getAllZipOnDevice();
                break;
        }

        ArrayList<FileData> resultList = new ArrayList<>();
        for (String filePath : fileList) {
            File file = new File(filePath);
            Uri uri = Uri.fromFile(file);
            int size = Integer.parseInt(String.valueOf(file.length() / 1024));

            FileData fileData = new FileData(getFileName(filePath), filePath, uri, (int) (file.lastModified() / 1000), size, getFileType(file));
            resultList.add(fileData);
        }

        FileSortUtils.performSortOperation(order, resultList);

        return resultList;
    }

    public static ArrayList<FileData> getFileListOfDirectory(FileData dirFileData, boolean isScanAll) {
        String dirPath = dirFileData.getFilePath();

        File dir = new File(dirPath);
        if (!dir.isDirectory()) {
            return new ArrayList<>();
        }

        File[] listFile = dir.listFiles();
        ArrayList<FileData> resultList = new ArrayList<>();

        if (listFile != null) {
            for (File file : listFile) {
                FileData fileData = new FileData();
                fileData.setFilePath(file.getAbsolutePath());
                fileData.setParentFile(dirFileData);
                fileData.setDateAdded((int) (file.lastModified() / 1000));
                fileData.setDisplayName(getFileName(file.getAbsolutePath()));

                if (file.isDirectory()) {
                    fileData.setFileType(DataConstants.FILE_TYPE_DIRECTORY);
                    if (file.listFiles() != null) {
                        fileData.setNumberChild(file.listFiles().length);
                    }
                } else {
                    fileData.setSize(Integer.parseInt(String.valueOf(file.length() / 1024)));
                    fileData.setFileUri(Uri.fromFile(file));

                    String fileType = getFileType(file);
                    if (isScanAll || fileType.length() > 0) {
                        fileData.setFileType(fileType);
                    } else {
                        continue;
                    }
                }

                resultList.add(fileData);
            }
        }

        if (!isScanAll) {
            FileSortUtils.performSortOperation(SORT_BY_TYPE, resultList);
        } else {
            FileSortUtils.performSortOperation(SORT_BY_TYPE_REVERT, resultList);
        }

        return resultList;
    }

    public static ArrayList<FileData> getNotEmptyFolderList(Context context) {
        File dir = Environment.getExternalStorageDirectory();
        if (!dir.isDirectory()) {
            return new ArrayList<>();
        }

        DirectoryUtils directoryUtils = new DirectoryUtils(context);
        List<String> allFilesList = directoryUtils.getAllDocumentsOnDevice();
        Map<String, Integer> mapFileNumber = new HashMap<>();

        for (String filePath : allFilesList) {
            String parentPath = filePath.substring(0, filePath.lastIndexOf("/"));
            if (mapFileNumber.containsKey(parentPath)) {
                mapFileNumber.put(parentPath, mapFileNumber.get(parentPath) + 1);
            } else {
                mapFileNumber.put(parentPath, 1);
            }
        }

        if (mapFileNumber.size() > 0) {
            ArrayList<FileData> resultList = new ArrayList<>();
            int totalNumberFile = 0;
            for (Map.Entry<String, Integer> entry : mapFileNumber.entrySet()) {
                String parentPath = entry.getKey();
                String parentName;
                if (parentPath.equals(dir.getAbsolutePath())) {
                    parentName = "root";
                } else {
                    parentName = getFileName(parentPath);
                }
                FileData fileData = new FileData();
                fileData.setDisplayName(parentName);
                fileData.setFilePath(parentPath);
                fileData.setNumberChild(entry.getValue());
                fileData.setFileType(DataConstants.FILE_TYPE_DIRECTORY);
                resultList.add(fileData);
                totalNumberFile += entry.getValue();
            }

            FileSortUtils.performSortOperation(SORT_BY_NAME_ASC, resultList);

            FileData root = new FileData(DataConstants.ALL_DOCUMENTS, dir.getAbsolutePath(), null, 0, 0, DataConstants.FILE_TYPE_DIRECTORY);
            root.setNumberChild(totalNumberFile);
            resultList.add(0, root);

            return resultList;
        } else {
            return new ArrayList<>();
        }
    }

    public static ArrayList<FileData> getAllFileOfDirectory(Context context, FileData parent, int sortBy, String filterType) {
        File file = new File(parent.getFilePath());
        if (!file.exists()) {
            return new ArrayList<>();
        }

        DirectoryUtils directoryUtils = new DirectoryUtils(context);
        List<String> validChildFiles = directoryUtils.getAllDocumentsOnDirectory(file, filterType);

        if (validChildFiles.size() == 0) {
            return new ArrayList<>();
        }

        ArrayList<FileData> resultList = new ArrayList<>();
        for (String filePath : validChildFiles) {
            File documentFile = new File(filePath);
            Uri uri = Uri.fromFile(documentFile);
            int size = Integer.parseInt(String.valueOf(documentFile.length() / 1024));

            String fileType = getFileType(documentFile);
            FileData fileData = new FileData(getFileName(filePath), filePath, uri, (int) (documentFile.lastModified() / 1000), size, fileType);
            fileData.setParentFile(parent);

            resultList.add(fileData);
        }

        FileSortUtils.performSortOperation(sortBy, resultList);

        return resultList;
    }

    public static String getFileType(File file) {
        if (!file.exists()) return "";

        if (file.getName().toLowerCase().endsWith(DataConstants.PDF_EXTENSION)) {
            return DataConstants.FILE_TYPE_PDF;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.DOC_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.DOCX_EXTENSION)) {
            return DataConstants.FILE_TYPE_WORD;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.EXCEL_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.EXCEL_WORKBOOK_EXTENSION)) {
            return DataConstants.FILE_TYPE_EXCEL;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.PPT_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.PPTX_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.POT_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.PPTM_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.POTX_EXTENSION) ||
                file.getName().toLowerCase().endsWith(DataConstants.POTM_EXTENSION)) {
            return DataConstants.FILE_TYPE_PPT;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.TEXT_EXTENSION)) {
            return DataConstants.FILE_TYPE_TXT;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.EXCEL_CSV_EXTENSION)) {
            return DataConstants.FILE_TYPE_CSV;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.RTF_EXTENSION)) {
            return DataConstants.FILE_TYPE_RTF;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.RAR_EXTENSION)) {
            return DataConstants.FILE_TYPE_RAR;
        } else if (file.getName().toLowerCase().endsWith(DataConstants.ZIP_EXTENSION)) {
            return DataConstants.FILE_TYPE_ZIP;
        } else {
            return "";
        }
    }

    public static void printFile(Context context, final File file) {
        try {
            final PrintDocumentAdapter mPrintDocumentAdapter = new PrintDocumentAdapterHelper(file);

            PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
            String jobName = context.getString(R.string.app_name) + " Print Document";

            if (printManager != null) {
                printManager.print(jobName, mPrintDocumentAdapter, null);
            } else {
                ToastUtils.showMessageShort(context, "Can not print file now.");
            }
        } catch (Exception e) {
            ToastUtils.showMessageShort(context, "Can not print file now.");
        }
    }

    public static void shareFile(Context context, File file) {
        String fileType = getFileType(file);
        String fileMime;

        switch (fileType) {
            case DataConstants.FILE_TYPE_PDF:
                fileMime = context.getString(R.string.pdf_type);
                break;
            case DataConstants.FILE_TYPE_EXCEL:
            case DataConstants.FILE_TYPE_WORD:
            case DataConstants.FILE_TYPE_PPT:
            case DataConstants.FILE_TYPE_CSV:
            case DataConstants.FILE_TYPE_TXT:
            case DataConstants.FILE_TYPE_RTF:
                fileMime = context.getString(R.string.excel_type);
                break;
            case DataConstants.FILE_TYPE_ZIP:
                fileMime = context.getString(R.string.zip_type);
                break;
            case DataConstants.FILE_TYPE_RAR:
                fileMime = context.getString(R.string.rar_type);
                break;
            default:
                fileMime = context.getString(R.string.word_type);
                break;
        }

        Uri uri = FileProvider.getUriForFile(context, AUTHORITY_APP, file);
        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(uri);

        shareFileWithType(context, uris, fileMime);
    }

    public static void shareMultipleFiles(Context context, List<File> files) {
        ArrayList<Uri> uris = new ArrayList<>();
        for (File file : files) {
            Uri uri = FileProvider.getUriForFile(context, AUTHORITY_APP, file);
            uris.add(uri);
        }
        shareFileWithType(context, uris, context.getString(R.string.pdf_type));
    }

    private static void shareFileWithType(Context context, ArrayList<Uri> uris, String type) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_file_title));
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType(type);

        try {
            context.startActivity(Intent.createChooser(intent, context.getResources().getString(R.string.share_chooser)));
        } catch (Exception e) {
            ToastUtils.showMessageShort(context, "Can not share file now.");
        }
    }

    public static void uploadFile(Activity context, File file) {
        Uri uri = FileProvider.getUriForFile(context, AUTHORITY_APP, file);
        String fileType = getFileType(file);
        String fileMime;

        switch (fileType) {
            case DataConstants.FILE_TYPE_PDF:
                fileMime = context.getString(R.string.pdf_type);
                break;
            case DataConstants.FILE_TYPE_EXCEL:
            case DataConstants.FILE_TYPE_WORD:
            case DataConstants.FILE_TYPE_PPT:
            case DataConstants.FILE_TYPE_CSV:
            case DataConstants.FILE_TYPE_TXT:
            case DataConstants.FILE_TYPE_RTF:
                fileMime = context.getString(R.string.excel_type);
                break;
            case DataConstants.FILE_TYPE_ZIP:
                fileMime = context.getString(R.string.zip_type);
                break;
            case DataConstants.FILE_TYPE_RAR:
                fileMime = context.getString(R.string.rar_type);
                break;
            default:
                fileMime = context.getString(R.string.word_type);
                break;
        }

        Intent uploadIntent = ShareCompat.IntentBuilder.from(context)
                .setText("Share Document")
                .setType(fileMime)
                .setStream(uri)
                .getIntent();
        try {
            context.startActivity(uploadIntent);
        } catch (Exception e) {
            ToastUtils.showMessageShort(context, "Can not upload file now.");
        }
    }

    public static void clearTempFolder(Context context) {
        File rootDir = context.getFilesDir();
        File containTempFileDir = new File(rootDir, DataConstants.TEMP_FOLDER);

        deleteRecursive(containTempFileDir);
    }

    public static void uploadTxtFile(Activity context, File file) {
        Uri uri = FileProvider.getUriForFile(context, AUTHORITY_APP, file);
        Intent uploadIntent = ShareCompat.IntentBuilder.from(context)
                .setText("Share Document")
                .setType("application/txt")
                .setStream(uri)
                .getIntent()
                .setPackage("com.google.android.apps.docs");

        try {
            context.startActivity(uploadIntent);
        } catch (Exception e) {
            ToastUtils.showMessageShort(context, "Can not upload file now.");
        }
    }

    public static void uploadImageFile(Activity context, File file) {
        Uri uri = FileProvider.getUriForFile(context, AUTHORITY_APP, file);
        Intent uploadIntent = ShareCompat.IntentBuilder.from(context)
                .setText("Share Image")
                .setType("image/png")
                .setStream(uri)
                .getIntent()
                .setPackage("com.google.android.apps.docs");

        try {
            context.startActivity(uploadIntent);
        } catch (Exception e) {
            ToastUtils.showMessageShort(context, "Can not upload file now.");
        }
    }

    public static void openFile(Context context, String path, String fileType) {
        if (path == null) {
            // TODO show error
            return;
        }
        String fileMimeType = "";

        if (fileType.equals(DataConstants.FILE_TYPE_RAR)) {
            fileMimeType = context.getString(R.string.rar_type);
        } else if (fileType.equals(DataConstants.FILE_TYPE_ZIP)) {
            fileMimeType = context.getString(R.string.zip_type);
        } else {
            fileMimeType = context.getString(R.string.image_type);
        }

        openFileInternal(context, path, fileMimeType);
    }

    private static void openFileInternal(Context context, String path, String dataType) {
        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        try {
            Uri uri = FileProvider.getUriForFile(context, AUTHORITY_APP, file);

            target.setDataAndType(uri, dataType);
            target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(target, context.getString(R.string.open_file)));
        } catch (Exception e) {
            ToastUtils.showMessageShort(context, context.getString(R.string.open_file_error));
        }
    }

    public static String getFileName(Context context, Uri uri) {
        String fileName = "File name";
        String scheme = uri.getScheme();

        if (scheme == null)
            return null;

        if (scheme.equals("file")) {
            return uri.getLastPathSegment();
        } else if (scheme.equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);

            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME);
                    cursor.moveToFirst();
                    fileName = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        }

        return fileName;
    }

    public static String getFileName(String path) {
        if (path == null)
            return "File name";

        int index = path.lastIndexOf("/");
        return index < path.length() ? path.substring(index + 1) : "File name";
    }

    public static int getNumberPages(String filePath) {
        ParcelFileDescriptor fileDescriptor = null;
        try {
            if (filePath != null)
                fileDescriptor = ParcelFileDescriptor.open(new File(filePath), MODE_READ_ONLY);
            if (fileDescriptor != null) {
                PdfRenderer renderer = new PdfRenderer(fileDescriptor);
                return renderer.getPageCount();
            }
        } catch (Exception ignored) {
        }

        return 0;
    }

    private static int checkRepeat(String finalOutputFile, final List<File> mFile, String extension) {
        boolean flag = true;
        int append = 0;
        while (flag) {
            append++;
            String name = finalOutputFile.replace(extension,
                    "(" + append + ")" + extension);
            flag = mFile.contains(new File(name));
        }

        return append;
    }

    public static void deleteFileOnExist(String path) {
        if (path == null) return;

        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception ignored) {
        }
    }

    public static boolean checkFileExist(String path) {
        if (path == null || path.length() == 0) return false;

        try {
            File file = new File(path);
            return file.exists();
        } catch (Exception ignored) {
        }

        return false;
    }

    public static boolean checkFileExistAndType(String path, FileType fileType) {
        if (path == null || path.length() == 0) return false;

        try {
            File file = new File(path);
            if (!file.exists()) {
                return false;
            }

            path = path.toLowerCase();

            switch (fileType) {
                case type_PDF:
                    return path.endsWith(".pdf");
                case type_WORD:
                    return path.endsWith(".txt") || path.endsWith(".doc") || path.endsWith(".docx");
                case type_EXCEL:
                    return path.endsWith(".xls") || path.endsWith(".xlsx");
                case type_IMAGE:
                    return path.endsWith(".jpeg") || path.endsWith(".jpg") || path.endsWith(".png");
                case type_PPT:
                    return path.endsWith(".ppt") || path.endsWith(".pptx");

            }
        } catch (Exception ignored) {
        }

        return false;
    }

    public static int renameFile(SavedData fileData, String newName) {
        try {
            File currentFile = new File(fileData.getFilePath());

            if (!fileData.getFilePath().contains(fileData.getDisplayName())) {
                return -2;
            }

            String newDir = fileData.getFilePath().replace(fileData.getDisplayName(), newName);
            File newFile = new File(newDir);

            if (newFile.exists()) {
                return -1;
            }

            if (!currentFile.exists()) {
                return -2;
            }

            if (rename(currentFile, newFile)) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return -2;
        }
    }

    public static int renameFile(FileData fileData, String newName) {
        try {
            File currentFile = new File(fileData.getFilePath());

            if (!fileData.getFilePath().contains(fileData.getDisplayName())) {
                return -2;
            }

            String newDir = fileData.getFilePath().replace(fileData.getDisplayName(), newName);
            File newFile = new File(newDir);

            if (newFile.exists()) {
                return -1;
            }

            if (!currentFile.exists()) {
                return -2;
            }

            if (rename(currentFile, newFile)) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return -2;
        }
    }

    private static boolean rename(File from, File to) {
        try {
            return from.getParentFile() != null && from.getParentFile().exists() && from.exists() && from.renameTo(to);
        } catch (Exception e) {
            return false;
        }
    }

    public static void deleteRecursive(File fileOrDirectory) {
        try {
            if (fileOrDirectory.isDirectory()) {
                for (File child : fileOrDirectory.listFiles()) {
                    deleteRecursive(child);
                }
            }

            fileOrDirectory.delete();
        } catch (Exception ignored) {
        }
    }

    public static String exactFile(Context context, String filePath) {
        if (!checkFileExist(filePath)) {
            return null;
        }

        String fileName = getFileName(filePath);
        File root = context.getFilesDir();
        File directory = new File(root, fileName);
        if (directory.exists()) {
            deleteRecursive(directory);
        }

        if (!directory.isDirectory() || !directory.exists()) {
            try {
                if (!directory.mkdir()) {
                    return null;
                }
            } catch (Exception ignored) {
                return null;
            }
        }

        if (filePath.toLowerCase().endsWith(DataConstants.RAR_EXTENSION)) {
            try {
                final File rar = new File(filePath);
                Junrar.extract(rar, directory);

                return directory.getAbsolutePath();
            } catch (Exception e) {
            }

            return null;
        } else {
            try {
                ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(filePath)));

                ZipEntry zipEntry;
                int count;
                byte[] buffer = new byte[8192];
                while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                    File file = new File(directory, zipEntry.getName());
                    File dir = zipEntry.isDirectory() ? file : file.getParentFile();
                    if (!dir.isDirectory() && !dir.mkdirs())
                        continue;
                    if (zipEntry.isDirectory())
                        continue;
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    try {
                        while ((count = zipInputStream.read(buffer)) != -1)
                            fileOutputStream.write(buffer, 0, count);
                    } finally {
                        fileOutputStream.close();
                    }
                }

                return directory.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
