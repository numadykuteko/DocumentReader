package com.documents.reader.ppt.txt.pdf.viewer.data.local.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.documents.reader.ppt.txt.pdf.viewer.data.local.database.dao.RecentDataDao;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;

@Database(entities = {RecentData.class}, version = 1, exportSchema = false)
public abstract class ApplicationDatabase extends RoomDatabase {
    public abstract RecentDataDao recentDataDao();
}
