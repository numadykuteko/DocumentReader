package com.documents.reader.ppt.txt.pdf.viewer.ui.component;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.ads.control.Admod;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;

public class ExitConfirmDialog extends Dialog {

    private Context mContext;
    private ConfirmListener mListener;

    public ExitConfirmDialog(@NonNull Context context, ConfirmListener listener) {
        super(context);
        mContext = context;
        mListener = listener;

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm_exit);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.96);
        getWindow().setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);

        Button cancelBtn = findViewById(R.id.btn_cancel);
        Button submitBtn = findViewById(R.id.btn_ok);

        cancelBtn.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onCancel();
            }
            dismiss();
        });
        submitBtn.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onSubmit();
            }
            dismiss();
        });

        setOnShowListener(dialogInterface -> {
            Admod.getInstance().loadNativeFragment((Activity) context, BuildConfig.native_id, findViewById(R.id.native_ads));
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public interface ConfirmListener {
        void onSubmit();
        void onCancel();
    }
}
