package com.documents.reader.ppt.txt.pdf.viewer.utils.ads;

import android.content.Context;

import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseRemoteUtils;

public class AdsShowCountMyPdfManager {
    private static final String LOG = "AdsShowCountMyPdfManager";

    private static AdsShowCountMyPdfManager mInstance;
    private int mCountForClickItem;

    private AdsShowCountMyPdfManager() {
        mCountForClickItem = 1;
    }

    public static AdsShowCountMyPdfManager getInstance() {
        if (mInstance == null) {
            mInstance = new AdsShowCountMyPdfManager();
        }

        return mInstance;
    }


    public boolean checkShowAdsForClickItem() {
        FirebaseRemoteUtils firebaseRemoteUtils = new FirebaseRemoteUtils();
        return mCountForClickItem >= firebaseRemoteUtils.getNumberTimeShowAdsOnce();
    }

    public void increaseCountForClickItem() {
        FirebaseRemoteUtils firebaseRemoteUtils = new FirebaseRemoteUtils();
        if (mCountForClickItem >= firebaseRemoteUtils.getNumberTimeShowAdsOnce()) {
            mCountForClickItem = 1;
        } else {
            mCountForClickItem ++;
        }
    }

}
