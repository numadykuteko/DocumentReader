package com.documents.reader.ppt.txt.pdf.viewer.utils.file;

import android.annotation.SuppressLint;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

public class FileInfoUtils {

    // GET PDF DETAILS
    public static String getFormattedDate(File file) {
        try {
            Date lastModDate = new Date(file.lastModified());
            String[] formatDate = lastModDate.toString().split(" ");
            String time = formatDate[3];
            String[] formatTime = time.split(":");
            String date = formatTime[0] + ":" + formatTime[1];

            return DateFormat.getDateInstance(DateFormat.MEDIUM).format(lastModDate) + " • " + date;
        } catch (Exception e) {
            return "";
        }

    }

    @SuppressLint("DefaultLocale")
    public static String getFormattedSize(File file) {
        if (file.length() < 100) {
            return String.format("%.2f B", (double) file.length());
        } else if (file.length() / 1024 < 100) {
            return String.format("%.2f KB", (double) file.length() / 1024);
        }
        return String.format("%.2f MB", (double) file.length() / (1024 * 1024));
    }
}
