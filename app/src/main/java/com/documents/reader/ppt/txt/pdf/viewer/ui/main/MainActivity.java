package com.documents.reader.ppt.txt.pdf.viewer.ui.main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.AppConstants;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.DataManager;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.UpdateInfo;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.ActivityMainBinding;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseBindingActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.browser.BrowserFragment;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.DataOptionDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.ExitConfirmDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.NotRequireUpdateDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.UpdateDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.folder.FolderFragment;
import com.documents.reader.ppt.txt.pdf.viewer.ui.lib.LibFragment;
import com.documents.reader.ppt.txt.pdf.viewer.ui.recent.RecentFragment;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseRemoteUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ads.FileViewerOnceManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainActivity extends BaseBindingActivity<ActivityMainBinding, MainViewModel> implements MainNavigator {

    public static final String FOLDER = "FOLDER";
    public static final String FILES = "FILES";
    public static final String HISTORY = "HISTORY";
    public static final String BROWSES = "EXPLORER";

    public static final List<String> mListScreenId = Arrays.asList(FOLDER, FILES, HISTORY, BROWSES);

    private int mCurrentScreen;

    private ActionBarDrawerToggle mDrawerToggle;

    private MainViewModel mMainViewModel;
    private ActivityMainBinding mActivityMainBinding;

    private int mSelectedItem = 0;
    private MenuItem mCurrentItem;
    private BrowserFragment mBrowserFragment;
    private FolderFragment mFolderFragment;
    private LibFragment mLibFragment;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        return mMainViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainViewModel.setNavigator(this);
        mActivityMainBinding = getViewDataBinding();

        DataManager.getInstance(this).increaseBackTime();

        initView();
    }

    @Override
    public void onBackPressed() {
        try {
            if (mActivityMainBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                mActivityMainBinding.drawerLayout.closeDrawer(GravityCompat.START);
            } else if (mCurrentScreen == 3) {
                if (mBrowserFragment != null) {
                    if (!mBrowserFragment.isCurrentFolderRoot()) {
                        mBrowserFragment.onBackPress();
                    } else {
                        mSelectedItem = 0;

                        if (mCurrentItem != null) {
                            mCurrentItem.setChecked(false);
                        }

                        mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_folder_view);
                        if (mCurrentItem != null) {
                            mCurrentItem.setCheckable(false);
                            mCurrentItem.setChecked(true);
                        }

                        goToScreen(FOLDER);
                    }
                } else {
                    restartApp(false);
                }
            } else if (mCurrentScreen == 0) {
                if (mFolderFragment != null) {
                    if (mFolderFragment.isFabOpen()) {
                        mFolderFragment.closeFab();
                    } else if (!mFolderFragment.isCurrentFolderRoot()) {
                        mFolderFragment.onBackPress();
                    } else {
                        submitExitApplication();
                    }
                } else {
                    restartApp(false);
                }
            } else if (mCurrentScreen == 1 && mLibFragment != null && mLibFragment.isFabOpen()) {
                mLibFragment.closeFab();
            } else {
                mSelectedItem = 0;

                if (mCurrentItem != null) {
                    mCurrentItem.setChecked(false);
                }

                mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_folder_view);
                if (mCurrentItem != null) {
                    mCurrentItem.setCheckable(false);
                    mCurrentItem.setChecked(true);
                }

                goToScreen(FOLDER);
            }
        } catch (Exception e) {
            restartApp(false);
        }
    }

    private void submitExitApplication() {
        DataManager dataManager = DataManager.getInstance(this);
        if (!dataManager.checkRatingUsDone()) {
            showRatingUsPopup(MainActivity.super::onBackPressed, MainActivity.super::onBackPressed, this::gotoExitAds);
        } else {
            ExitConfirmDialog exitConfirmDialog = new ExitConfirmDialog(this, new ExitConfirmDialog.ConfirmListener() {
                @Override
                public void onSubmit() {
                    gotoExitAds();
                }

                @Override
                public void onCancel() {

                }
            });
            exitConfirmDialog.show();
        }
    }

    private void gotoExitAds() {
        if (FileViewerOnceManager.getInstance().checkFileViewerOnce()) {
            showExitAppAds(() -> {
                Intent intent = new Intent(MainActivity.this, AfterAdsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
                finish();
            });
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1) {
            if (resultCode == BaseBindingActivity.RESULT_NEED_FINISH) {
                finish();
                setResult(RESULT_NEED_FINISH);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void initView() {
        setTabView();

        preloadMyPdfAdsIfInit();
        preloadExitAdsIfInit();

        mActivityMainBinding.searchToolbar.setOnClickListener(view -> gotoActivityWithFlag(AppConstants.FLAG_SEARCH_PDF));

        checkForUpdateApp();
    }

    @Override
    protected void setClick() {
    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void checkForUpdateApp() {
        FirebaseRemoteUtils firebaseRemoteUtils = new FirebaseRemoteUtils();
        firebaseRemoteUtils.fetchRemoteConfig(this, () -> getUpdateInfo(firebaseRemoteUtils));
    }

    private void getUpdateInfo(FirebaseRemoteUtils firebaseRemoteUtils) {
        UpdateInfo updateInfo = firebaseRemoteUtils.getUpdateInfo();

        if (updateInfo != null) {
            if (updateInfo.getVersionCode() > BuildConfig.VERSION_CODE && updateInfo.getStatus()) {
                if (updateInfo.getRequiredUpdateList().contains(BuildConfig.VERSION_CODE) && updateInfo.isIsRequired()) {
                    UpdateDialog updateDialog = new UpdateDialog(this, updateInfo.getVersionName(), this::gotoRateUsActivity);
                    updateDialog.show();
                } else {
                    NotRequireUpdateDialog notRequireUpdateDialog = new NotRequireUpdateDialog(updateInfo.getVersionName(), this::gotoRateUsActivity);
                    notRequireUpdateDialog.show(getSupportFragmentManager(), notRequireUpdateDialog.getTag());
                }
            }
        }
    }

    public void setClickFilterItem(View.OnClickListener listener) {
        FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Filter");

        mActivityMainBinding.filterToolbar.setOnClickListener(listener);
    }

    public void gotoFeedBackApplication() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{DataConstants.EMAIL_DEV});
        startActivity(intent);
    }

    public void gotoMoreApp() {
        String url = "https://play.google.com/store/apps/developer?id=Andromeda+App";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void gotoPolicyApp() {
        String url = "https://andromedstudio.github.io/pdfReader/Privacy_Policy.html";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void shareApplicationLink() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            String shareMessage = "\nLet me recommend you this Office Reader application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "Choose one"));
        } catch (Exception e) {
            ToastUtils.showMessageShort(this, getString(R.string.can_not_connect_to_network_text));
        }
    }

    private void chooseLanguage() {
        FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Language");

        String[] LANG_CODE_LIST = {"es", "vi", "kr", "ja", "fr", "hi", "pt", "id", "zh"};
        String[] LANGUAGE_LIST = {getString(R.string.english), getString(R.string.vietnamese), getString(R.string.korean),
                getString(R.string.japanese), getString(R.string.french), getString(R.string.hindi), getString(R.string.portuguese),
                getString(R.string.indonesia), getString(R.string.chinese)};

        String appLocaleString = DataManager.getInstance(this).getAppLocale();
        Locale appLocale;

        if (appLocaleString == null) {
            appLocale = getResources().getConfiguration().locale;
        } else {
            appLocale = new Locale(appLocaleString);
        }

        String selectedLanguage = LANGUAGE_LIST[0];
        for (int i = 0; i < LANG_CODE_LIST.length; i++) {
            if (LANG_CODE_LIST[i].equals(appLocale.getLanguage())) {
                selectedLanguage = LANGUAGE_LIST[i];
                break;
            }
        }

        DataOptionDialog dataOptionDialog = new DataOptionDialog(this, getString(R.string.menu_drawer_language), LANGUAGE_LIST, selectedLanguage, newOption -> {
            String newLanguage = LANG_CODE_LIST[newOption];
            if (newLanguage.equals(appLocale.getLanguage())) {
                return;
            }

            DataManager.getInstance(this).setAppLocale(newLanguage);

            Locale locale = new Locale(newLanguage);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

            restartApp(false);
        }, false);
        dataOptionDialog.show();
    }

    private void chooseTheme() {
        FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Theme");

        String[] THEME_LIST = {getString(R.string.red), getString(R.string.blue), getString(R.string.jade), getString(R.string.violet), getString(R.string.orange), getString(R.string.green), getString(R.string.yellow)};
        int selectedThemeIndex = DataManager.getInstance(this).getTheme();
        String selectedTheme = THEME_LIST[selectedThemeIndex];

        DataOptionDialog dataOptionDialog = new DataOptionDialog(this, getString(R.string.menu_drawer_select_theme), THEME_LIST, selectedTheme, newOption -> {
            String newTheme = THEME_LIST[newOption];
            if (newTheme.equals(selectedTheme)) {
                return;
            }

            DataManager.getInstance(this).setTheme(newOption);
            restartApp(false);
        }, true);
        dataOptionDialog.show();
    }

    @SuppressLint({"RtlHardcoded", "SetTextI18n"})
    private void setTabView() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mActivityMainBinding.drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mActivityMainBinding.drawerLayout.addDrawerListener(mDrawerToggle);
        mActivityMainBinding.drawerLayout.setDrawerElevation(0);

        mDrawerToggle.syncState();

        mActivityMainBinding.menuImg.setOnClickListener(view -> {
            try {
                mActivityMainBinding.drawerLayout.openDrawer(Gravity.LEFT);
            } catch (Exception e) {
                Log.d("MainActivity", "Can not open drawer");
            }
        });

        mSelectedItem = 0;

        mActivityMainBinding.navView.setItemIconTintList(null);

        mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_folder_view);
        MenuItem fileItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_file_view);
        MenuItem recentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_recent_view);
        MenuItem browsesItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_explorer_view);

        if (mCurrentItem != null) {
            mCurrentItem.setCheckable(false);
            mCurrentItem.setChecked(true);
        }

        if (fileItem != null) {
            fileItem.setCheckable(false);
        }

        if (recentItem != null) {
            recentItem.setCheckable(false);
        }

        if (browsesItem != null) {
            browsesItem.setCheckable(false);
        }

        goToScreen(FOLDER);

        mActivityMainBinding.navView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.navigation_folder_view) {
                if (mSelectedItem != 0) {
                    mSelectedItem = 0;
                    if (mCurrentItem != null) {
                        mCurrentItem.setChecked(false);
                    }

                    mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_folder_view);
                    if (mCurrentItem != null) {
                        mCurrentItem.setChecked(true);
                    }

                    goToScreen(FOLDER);
                }
            } else if (id == R.id.navigation_file_view) {
                gotoFileView();
            } else if (id == R.id.navigation_recent_view) {
                if (mSelectedItem != 2) {
                    mSelectedItem = 2;
                    if (mCurrentItem != null) {
                        mCurrentItem.setChecked(false);
                    }

                    mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_recent_view);
                    if (mCurrentItem != null) {
                        mCurrentItem.setChecked(true);
                    }

                    goToScreen(HISTORY);
                }
            } else if (id == R.id.navigation_explorer_view) {
                if (mSelectedItem != 3) {
                    mSelectedItem = 3;
                    if (mCurrentItem != null) {
                        mCurrentItem.setChecked(false);
                    }

                    mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_explorer_view);
                    if (mCurrentItem != null) {
                        mCurrentItem.setChecked(true);
                    }

                    goToScreen(BROWSES);
                }
            } else if (id == R.id.navigation_drawer_language) {
                chooseLanguage();
            } else if (id == R.id.navigation_drawer_theme) {
                chooseTheme();
            }  else if (id == R.id.navigation_drawer_privacy) {
                gotoPolicyApp();
            } else if (id == R.id.navigation_drawer_feedback) {
                gotoFeedBackApplication();
            } else if (id == R.id.navigation_drawer_rate) {
                showRatingUsPopup(() -> {
                }, () -> {
                }, () -> {
                });
            } else if (id == R.id.navigation_drawer_share) {
                shareApplicationLink();
            }
            mActivityMainBinding.drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        });
    }

    public void gotoFileView() {
        if (mSelectedItem != 1) {
            mSelectedItem = 1;
            if (mCurrentItem != null) {
                mCurrentItem.setChecked(false);
            }

            mCurrentItem = mActivityMainBinding.navView.getMenu().findItem(R.id.navigation_file_view);
            if (mCurrentItem != null) {
                mCurrentItem.setChecked(true);
            }

            goToScreen(FILES);
        }
    }

    public void setVisibleBtnToolbar(int visibleSortToolbar) {
        mActivityMainBinding.filterToolbar.setVisibility(visibleSortToolbar);
    }

    public void setToolbarName(String name) {
        if (name == null) {
            mActivityMainBinding.titleToolbar.setText(getString(R.string.menu_drawer_folder_view));
        } else {
            mActivityMainBinding.titleToolbar.setText(name);
        }
    }

    public void goToScreen(String screenId) {
        int indexScreen = mListScreenId.indexOf(screenId);
        if (indexScreen == -1) return;

        if (indexScreen < mListScreenId.size()) {
            mCurrentScreen = indexScreen;

            setVisibleBtnToolbar(View.GONE);
            if (indexScreen == 0) {
                FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Folder");
                mActivityMainBinding.titleToolbar.setText(getString(R.string.menu_drawer_folder_view));
                mFolderFragment = FolderFragment.newInstance();
                setVisibleBtnToolbar(View.VISIBLE);
                loadFragment(mFolderFragment);
            } else if (indexScreen == 1) {
                FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_File");
                mActivityMainBinding.titleToolbar.setText(getString(R.string.menu_drawer_file_view));
                mLibFragment = LibFragment.newInstance();
                setVisibleBtnToolbar(View.VISIBLE);
                loadFragment(mLibFragment);
            } else if (indexScreen == 2) {
                FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Recent");
                mActivityMainBinding.titleToolbar.setText(getString(R.string.menu_drawer_recent_view));
                setVisibleBtnToolbar(View.VISIBLE);
                loadFragment(RecentFragment.newInstance());
            } else if (indexScreen == 3) {
                FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Browses");
                mActivityMainBinding.titleToolbar.setText(getString(R.string.menu_drawer_explorer_view));
                mBrowserFragment = BrowserFragment.newInstance();
                loadFragment(mBrowserFragment);
            }
        }
    }

    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();
    }
}
