package com.documents.reader.ppt.txt.pdf.viewer.ui.recent;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ads.control.Admod;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.RecentData;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.FragmentRecentBinding;
import com.documents.reader.ppt.txt.pdf.viewer.listener.OnFileItemWithOptionClickListener;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseFragment;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.ConfirmDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.FileOptionDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.RenameFileDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.main.MainActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.CommonUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DialogFactory;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.SnackBarUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.adapter.SaveListNoAdsAdapter;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RecentFragment extends BaseFragment<FragmentRecentBinding, RecentViewModel> implements RecentNavigator, OnFileItemWithOptionClickListener {

    private RecentViewModel mRecentViewModel;
    private FragmentRecentBinding mFragmentRecentBinding;
    private boolean mIsLoading;
    private List<RecentData> mListFile = new ArrayList<>();
    private SaveListNoAdsAdapter mFileListAdapter;

    private SweetAlertDialog mRequestPermissionDialog;
    private final int REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE = 1;

    private FileOptionDialog fileOptionDialog;

    private String mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_recent;
    }

    @Override
    public void reloadEasyChangeData() {

    }

    public void reloadData(boolean isForceReload) {
        if (mActivity != null && mActivity.notHaveStoragePermission()) {
            mFragmentRecentBinding.pullToRefresh.setRefreshing(false);

            showPermissionIssueArea();
            mIsLoading = false;
            return;
        }

        if (mIsLoading) return;

        mIsLoading = true;

        if (mListFile == null || mListFile.size() == 0 || isForceReload) {
            if (mFragmentRecentBinding != null) {
                showLoadingArea();
            }
        }

        mRecentViewModel.getFileList(mFilterType);
    }

    @Override
    public RecentViewModel getViewModel() {
        mRecentViewModel = ViewModelProviders.of(this).get(RecentViewModel.class);
        return mRecentViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mRecentViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentRecentBinding = getViewDataBinding();

        setForClick();
        initView();
        setForLiveData();

        reloadData(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE:
                if ((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    mRequestPermissionDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    mRequestPermissionDialog.setTitleText(getString(R.string.thankyou_text));
                    mRequestPermissionDialog.setContentText(getString(R.string.get_file_now));
                    mRequestPermissionDialog.showCancelButton(false);
                    mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                    mRequestPermissionDialog.setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismiss();
                        reloadData(true);
                    });
                } else {
                    mRequestPermissionDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    mRequestPermissionDialog.setTitleText(getString(R.string.title_need_permission_fail));
                    mRequestPermissionDialog.setContentText(getString(R.string.couldnt_get_file_now));
                    mRequestPermissionDialog.showCancelButton(false);
                    mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                    mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        if (mIsRequestFullPermission) {
            mIsRequestFullPermission = false;

            if (!mActivity.notHaveStoragePermission()) {
                mRequestPermissionDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                mRequestPermissionDialog.setTitleText(getString(R.string.thankyou_text));
                mRequestPermissionDialog.setContentText(getString(R.string.get_file_now));
                mRequestPermissionDialog.showCancelButton(false);
                mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
            } else {
                mRequestPermissionDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                mRequestPermissionDialog.setTitleText(getString(R.string.title_need_permission_fail));
                mRequestPermissionDialog.setContentText(getString(R.string.couldnt_get_file_now));
                mRequestPermissionDialog.showCancelButton(false);
                mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
            }
        }

        reloadData(false);
        super.onResume();
    }


    public static RecentFragment newInstance() {
        RecentFragment recentFragment = new RecentFragment();

        Bundle args = new Bundle();
        recentFragment.setArguments(args);
        recentFragment.setRetainInstance(true);

        return recentFragment;
    }

    private void initView() {
        Admod.getInstance().loadBanner(mActivity, BuildConfig.banner_id);

        mFragmentRecentBinding.pullToRefresh.setOnRefreshListener(() -> {
            reloadData(false);
        });

        mFileListAdapter = new SaveListNoAdsAdapter(this);
        mFragmentRecentBinding.dataListArea.setLayoutManager(new LinearLayoutManager(mActivity));
        mFragmentRecentBinding.dataListArea.setAdapter(mFileListAdapter);
    }

    private void setForClick() {
        if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).setClickFilterItem(view -> showFilterOptions(view));
        }
    }

    @SuppressLint("NonConstantResourceId")
    private void showFilterOptions(View view) {
        PopupMenu popup = new PopupMenu(mActivity, view);
        popup.inflate(R.menu.menu_filter_file);
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.all_type:
                    mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;
                    reloadData(true);
                    return false;
                case R.id.pdf_type:
                    mFilterType = DataConstants.FILE_TYPE_PDF;
                    reloadData(true);
                    return false;
                case R.id.word_type:
                    mFilterType = DataConstants.FILE_TYPE_WORD;
                    reloadData(true);
                    return false;
                case R.id.excel_type:
                    mFilterType = DataConstants.FILE_TYPE_EXCEL;
                    reloadData(true);
                    return false;
                case R.id.csv_type:
                    mFilterType = DataConstants.FILE_TYPE_CSV;
                    reloadData(true);
                    return false;
                case R.id.ppt_type:
                    mFilterType = DataConstants.FILE_TYPE_PPT;
                    reloadData(true);
                    return false;
                case R.id.txt_type:
                    mFilterType = DataConstants.FILE_TYPE_TXT;
                    reloadData(true);
                    return false;
                case R.id.rar_type:
                    mFilterType = DataConstants.FILE_TYPE_RAR;
                    reloadData(true);
                    return false;
                case R.id.zip_type:
                    mFilterType = DataConstants.FILE_TYPE_ZIP;
                    reloadData(true);
                    return false;
                default:
                    return false;
            }
        });

        CommonUtils.insertMenuItemIcons(mActivity, popup);
        popup.show();
    }

    private void setForLiveData() {
        mRecentViewModel.getListFileLiveData().observe(getViewLifecycleOwner(), this::updateData);
    }

    private void updateData(List<RecentData> recentDataList) {
        if (recentDataList.size() > 0) {
            if (recentDataList.equals(mListFile)) {
                mIsLoading = false;
                mFragmentRecentBinding.pullToRefresh.setRefreshing(false);

                return;
            }

            mListFile = new ArrayList<>();
            mListFile.addAll(recentDataList);

            Parcelable oldPosition = null;
            if (mFragmentRecentBinding.dataListArea.getLayoutManager() != null) {
                oldPosition = mFragmentRecentBinding.dataListArea.getLayoutManager().onSaveInstanceState();
            }
            mFileListAdapter.setData(mListFile);
            if (oldPosition != null) {
                mFragmentRecentBinding.dataListArea.getLayoutManager().onRestoreInstanceState(oldPosition);
            }
            showDataArea();
        } else {
            showNoDataArea();
        }

        mIsLoading = false;
        mFragmentRecentBinding.pullToRefresh.setRefreshing(false);
    }

    private void startRequestPermission() {
        if (mActivity != null && mActivity.notHaveStoragePermission()) {
            mRequestPermissionDialog = DialogFactory.getDialogRequestSomething(mActivity, getString(R.string.title_need_permission), getString(R.string.need_permission_to_get_file));
            mRequestPermissionDialog.setConfirmClickListener(sweetAlertDialog -> {
                requestReadStoragePermissionsSafely(REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE);
            });
            mRequestPermissionDialog.setCancelClickListener(sweetAlertDialog -> {
                sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(getString(R.string.title_need_permission_fail));
                sweetAlertDialog.setContentText(getString(R.string.couldnt_get_file_now));
                sweetAlertDialog.setConfirmClickListener(Dialog::dismiss);
                sweetAlertDialog.showCancelButton(false);
                sweetAlertDialog.setConfirmText(getString(R.string.confirm_text));
            });
            mRequestPermissionDialog.show();
        } else {
            reloadData(true);
        }
    }

    private void showNoDataArea() {
        if (mFragmentRecentBinding != null) {
            mFragmentRecentBinding.noDataErrorTv.setText(R.string.no_file_found);

            mFragmentRecentBinding.dataListArea.setVisibility(View.GONE);
            mFragmentRecentBinding.noDataErrorArea.setVisibility(View.VISIBLE);
            mFragmentRecentBinding.noPermissionArea.setVisibility(View.GONE);
            mFragmentRecentBinding.loadingArea.setVisibility(View.GONE);
        }
    }

    private void showPermissionIssueArea() {
        if (mFragmentRecentBinding != null) {
            mFragmentRecentBinding.noPermissionArea.setOnClickListener(v -> {
                startRequestPermission();
            });
            mFragmentRecentBinding.dataListArea.setVisibility(View.GONE);
            mFragmentRecentBinding.noDataErrorArea.setVisibility(View.GONE);
            mFragmentRecentBinding.noPermissionArea.setVisibility(View.VISIBLE);
            mFragmentRecentBinding.loadingArea.setVisibility(View.GONE);
        }
    }

    private void showDataArea() {
        if (mFragmentRecentBinding != null) {
            mFragmentRecentBinding.dataListArea.setVisibility(View.VISIBLE);
            mFragmentRecentBinding.noDataErrorArea.setVisibility(View.GONE);
            mFragmentRecentBinding.noPermissionArea.setVisibility(View.GONE);
            mFragmentRecentBinding.loadingArea.setVisibility(View.GONE);
        }
    }

    private void showLoadingArea() {
        if (mFragmentRecentBinding != null) {
            mFragmentRecentBinding.dataListArea.setVisibility(View.GONE);
            mFragmentRecentBinding.noDataErrorArea.setVisibility(View.GONE);
            mFragmentRecentBinding.loadingArea.setVisibility(View.VISIBLE);
            mFragmentRecentBinding.noPermissionArea.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClickItem(int position) {
        String filePath = mListFile.get(position).getFilePath();
        if (!FileUtils.checkFileExist(filePath)) {
            ToastUtils.showMessageShort(mActivity, getString(R.string.file_not_found));
            mRecentViewModel.clearSavedData(filePath);
            if (position >= 0 && position < mListFile.size()) {
                mListFile.remove(position);
            }

            mFileListAdapter.clearData(position);
            if (mListFile.size() == 0) {
                updateData(new ArrayList<>());
            }

            return;
        }

        if (mActivity != null) {
            mActivity.showViewFileAdsBeforeAction(() -> openDocumentFile(filePath));
        }
    }

    @Override
    public void onMainFunctionItem(int position) {
        onShowOption(position);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onOptionItem(int position) {
        onShowOption(position);
    }

    private void onShowOption(int position) {
        RecentData recentData = mListFile.get(position);

        hideOptionDialog();
        fileOptionDialog = new FileOptionDialog(recentData.getDisplayName(), recentData.getTimeAdded(), position, new FileOptionDialog.FileOptionListener() {
            @Override
            public void openFile(int position) {
                openPdfFile(position);
            }

            @Override
            public void shareFile(int position) {
                shareDocumentFile(recentData.getFilePath());
            }

            @Override
            public void printFile(int position) {
                printDocumentFile(recentData.getFilePath());
            }

            @Override
            public void infoFile(int position) {
                infoDocumentFile(recentData.getFilePath());
            }

            @Override
            public void renameFile(int position) {
                renamePdfFile(position);
            }

            @Override
            public void deleteFile(int position) {
                deletePdfFile(position);
            }
        });
        fileOptionDialog.show(getChildFragmentManager(), fileOptionDialog.getTag());
        CommonUtils.hideKeyboard(mActivity);
    }

    private void hideOptionDialog() {
        if (fileOptionDialog != null && fileOptionDialog.isVisible()) {
            fileOptionDialog.dismiss();
        }
    }

    private void openPdfFile(int position) {
        onClickItem(position);
    }

    private void renamePdfFile(int position) {
        FirebaseUtils.sendEventFunctionUsed(mActivity, FirebaseUtils.EVENT_NAME_LIST_FILE, "Rename file");

        RecentData recentData = mListFile.get(position);
        String displayName;
        String extension;
        try {
            displayName = recentData.getDisplayName().substring(0, recentData.getDisplayName().lastIndexOf("."));
            extension = recentData.getDisplayName().substring(recentData.getDisplayName().lastIndexOf("."));
        } catch (Exception e) {
            return;
        }

        RenameFileDialog renameFileDialog = new RenameFileDialog(mActivity, displayName, new RenameFileDialog.RenameFileListener() {
            @Override
            public void onSubmitName(String name) {
                String newName = name + extension;
                int result = FileUtils.renameFile(recentData, newName);

                if (result == -2 || result == 0) {
                    ToastUtils.showMessageShort(mActivity, getString(R.string.can_not_edit_video_name));
                } else if (result == -1) {
                    SnackBarUtils.getSnackbar(mActivity, getString(R.string.duplicate_video_name) + ": " + name).show();
                } else {
                    SnackBarUtils.getSnackbar(mActivity, getString(R.string.rename_file_success)).show();
                    String oldFilePath = recentData.getFilePath();

                    recentData.setFilePath(recentData.getFilePath().replace(recentData.getDisplayName(), newName));
                    recentData.setDisplayName(newName);
                    mListFile.set(position, recentData);
                    mFileListAdapter.updateData(position, recentData);

                    mRecentViewModel.updateSavedData(oldFilePath, recentData.getFilePath());
                }
            }

            @Override
            public void onCancel() {

            }
        });

        renameFileDialog.show();
    }

    private void deletePdfFile(int position) {
        FirebaseUtils.sendEventFunctionUsed(mActivity, FirebaseUtils.EVENT_NAME_LIST_FILE, "Delete file");
        RecentData recentData = mListFile.get(position);

        ConfirmDialog confirmDialog = new ConfirmDialog(mActivity, mActivity.getString(R.string.confirm_delete_file_title), mActivity.getString(R.string.confirm_delete_file_message), new ConfirmDialog.ConfirmListener() {
            @Override
            public void onSubmit() {
                if (!mActivity.notHaveStoragePermission()) {
                    FileUtils.deleteFileOnExist(recentData.getFilePath());
                    mRecentViewModel.clearSavedData(recentData.getFilePath());

                    if (position >= 0 && position < mListFile.size()) {
                        mListFile.remove(position);
                    }

                    mFileListAdapter.clearData(position);
                    if (mListFile.size() == 0) {
                        showNoDataArea();
                    }
                    SnackBarUtils.getSnackbar(mActivity, mActivity.getString(R.string.delete_success_text)).show();
                    hideOptionDialog();
                }
            }

            @Override
            public void onCancel() {

            }
        });
        confirmDialog.show();
    }

    private void removeFile(int position) {
        if (position >= 0 && position < mListFile.size()) {
            RecentData recentData = mListFile.get(position);
            mRecentViewModel.clearRecent(recentData.getFilePath());
            mListFile.remove(position);

            mFileListAdapter.clearData(position);
            if (mListFile.size() == 0) {
                showNoDataArea();
            }

            SnackBarUtils.getSnackbar(mActivity, mActivity.getString(R.string.recent_remove_success)).show();
        }
    }
}
