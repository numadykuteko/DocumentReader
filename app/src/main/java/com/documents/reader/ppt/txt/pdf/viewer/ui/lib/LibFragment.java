package com.documents.reader.ppt.txt.pdf.viewer.ui.lib;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ads.control.Admod;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.FragmentLibBinding;
import com.documents.reader.ppt.txt.pdf.viewer.listener.OnFileItemWithOptionClickListener;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseFragment;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.ConfirmDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.FileOptionDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.component.RenameFileDialog;
import com.documents.reader.ppt.txt.pdf.viewer.ui.main.MainActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ColorUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.CommonUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DialogFactory;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.SnackBarUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.adapter.FileListNoAdsWithOptionAdapter;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.RealPathUtil;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LibFragment extends BaseFragment<FragmentLibBinding, LibViewModel> implements LibNavigator, OnFileItemWithOptionClickListener {

    private LibViewModel mLibViewModel;
    private FragmentLibBinding mFragmentLibBinding;
    private int mCurrentSortBy = FileUtils.SORT_BY_DATE_DESC;
    private String mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;

    private boolean mIsLoading;
    private List<FileData> mListFile = new ArrayList<>();
    private FileListNoAdsWithOptionAdapter mFileListAdapter;

    private SweetAlertDialog mRequestPermissionDialog;
    private final int REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE = 1;

    private FileOptionDialog fileOptionDialog;

    private boolean mIsFabOpen = false;
    
    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_lib;
    }

    @Override
    public void reloadEasyChangeData() {

    }

    public void reloadData(boolean isForceReload) {
        if (mActivity != null && mActivity.notHaveStoragePermission()) {
            mFragmentLibBinding.pullToRefresh.setRefreshing(false);

            showPermissionIssueArea();
            mIsLoading = false;
            return;
        }

        if (mIsLoading) return;

        mIsLoading = true;

        if (mListFile == null || mListFile.size() == 0 || isForceReload) {
            showLoadingArea();
        }

        if (mActivity != null && mActivity instanceof MainActivity) {
            switch (mFilterType) {
                case DataConstants.FILE_TYPE_ALL_DOCUMENT:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.menu_drawer_file_view));
                    break;
                case DataConstants.FILE_TYPE_PDF:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.pdf_file));
                    break;
                case DataConstants.FILE_TYPE_WORD:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.word_file));
                    break;
                case DataConstants.FILE_TYPE_EXCEL:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.excel_file));
                    break;
                case DataConstants.FILE_TYPE_PPT:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.ppt_file));
                    break;
                case DataConstants.FILE_TYPE_RAR:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.rar_file));
                    break;
                case DataConstants.FILE_TYPE_RTF:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.rtf_file));
                    break;
                case DataConstants.FILE_TYPE_TXT:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.txt_file));
                    break;
                case DataConstants.FILE_TYPE_ZIP:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.zip_file));
                    break;
                case DataConstants.FILE_TYPE_CSV:
                    ((MainActivity) mActivity).setToolbarName(getString(R.string.csv_file));
                    break;

            }
        }

        mLibViewModel.getFileList(mCurrentSortBy, mFilterType);
    }

    @Override
    public LibViewModel getViewModel() {
        mLibViewModel = ViewModelProviders.of(this).get(LibViewModel.class);
        return mLibViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mLibViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentLibBinding = getViewDataBinding();

        setForClick();
        initView();
        setForLiveData();

        reloadData(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE:
                if ((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    mRequestPermissionDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    mRequestPermissionDialog.setTitleText(getString(R.string.thankyou_text));
                    mRequestPermissionDialog.setContentText(getString(R.string.get_file_now));
                    mRequestPermissionDialog.showCancelButton(false);
                    mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                    mRequestPermissionDialog.setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismiss();
                        reloadData(true);
                    });
                } else {
                    mRequestPermissionDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    mRequestPermissionDialog.setTitleText(getString(R.string.title_need_permission_fail));
                    mRequestPermissionDialog.setContentText(getString(R.string.couldnt_get_file_now));
                    mRequestPermissionDialog.showCancelButton(false);
                    mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                    mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        if (mIsRequestFullPermission) {
            mIsRequestFullPermission = false;

            if (!mActivity.notHaveStoragePermission()) {
                mRequestPermissionDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                mRequestPermissionDialog.setTitleText(getString(R.string.thankyou_text));
                mRequestPermissionDialog.setContentText(getString(R.string.get_file_now));
                mRequestPermissionDialog.showCancelButton(false);
                mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);

                reloadData(true);
            } else {
                mRequestPermissionDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                mRequestPermissionDialog.setTitleText(getString(R.string.title_need_permission_fail));
                mRequestPermissionDialog.setContentText(getString(R.string.couldnt_get_file_now));
                mRequestPermissionDialog.showCancelButton(false);
                mRequestPermissionDialog.setConfirmText(getString(R.string.confirm_text));
                mRequestPermissionDialog.setConfirmClickListener(Dialog::dismiss);
            }
        }

        super.onResume();
    }


    public static LibFragment newInstance() {
        LibFragment libFragment = new LibFragment();

        Bundle args = new Bundle();
        libFragment.setArguments(args);
        libFragment.setRetainInstance(true);

        return libFragment;
    }

    private void initView() {
        Admod.getInstance().loadBanner(mActivity, BuildConfig.banner_id);

        mFragmentLibBinding.pullToRefresh.setOnRefreshListener(() -> {
            reloadData(false);
        });

        mFileListAdapter = new FileListNoAdsWithOptionAdapter(this);
        mFragmentLibBinding.dataListArea.setLayoutManager(new LinearLayoutManager(mActivity));
        mFragmentLibBinding.dataListArea.setAdapter(mFileListAdapter);

        mIsFabOpen = false;
        setForFabSort();
    }

    public boolean isFabOpen() {
        return mIsFabOpen;
    }

    public void closeFab() {
        if (mIsFabOpen) {
            mIsFabOpen = false;
            updateFabStatus();
        }
    }
    
    private void setForFabSort() {
        mFragmentLibBinding.fabMenuSort.fabViewSort.setOnClickListener(view -> {
            if (mIsFabOpen) {
                mIsFabOpen = false;
                updateFabStatus();
            } else {
                mIsFabOpen = true;
                updateFabStatus();
            }
        });

        mFragmentLibBinding.fabMenuSort.textSortDateAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_ASC);
        });
        mFragmentLibBinding.fabMenuSort.fabSortDateAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_ASC);
        });

        mFragmentLibBinding.fabMenuSort.textSortDateDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_DESC);
        });
        mFragmentLibBinding.fabMenuSort.fabSortDateDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_DATE_DESC);
        });

        mFragmentLibBinding.fabMenuSort.textSortSizeAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_ASC);
        });
        mFragmentLibBinding.fabMenuSort.fabSortSizeAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_ASC);
        });

        mFragmentLibBinding.fabMenuSort.textSortSizeDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_DESC);
        });
        mFragmentLibBinding.fabMenuSort.fabSortSizeDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_SIZE_DESC);
        });

        mFragmentLibBinding.fabMenuSort.textSortNameAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_ASC);
        });
        mFragmentLibBinding.fabMenuSort.fabSortNameAsc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_ASC);
        });

        mFragmentLibBinding.fabMenuSort.textSortNameDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_DESC);
        });
        mFragmentLibBinding.fabMenuSort.fabSortNameDesc.setOnClickListener(v -> {
            setForSortValue(FileUtils.SORT_BY_NAME_DESC);
        });
    }

    private void setForSortValue(int newValue) {
        if (mIsLoading) {
            ToastUtils.showMessageShort(mActivity, getString(R.string.sort_not_available));
            return;
        }
        mCurrentSortBy = newValue;
        reloadData(true);

        mIsFabOpen = false;
        updateFabStatus();
    }
    
    private void updateFabStatus() {
        mFragmentLibBinding.fabMenuSort.fabSortDateAsc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.fabSortDateDesc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.fabSortNameAsc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.fabSortNameDesc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.fabSortSizeAsc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.fabSortSizeDesc.setClickable(mIsFabOpen);

        mFragmentLibBinding.fabMenuSort.textSortDateAsc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.textSortDateDesc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.textSortNameAsc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.textSortNameDesc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.textSortSizeAsc.setClickable(mIsFabOpen);
        mFragmentLibBinding.fabMenuSort.textSortSizeDesc.setClickable(mIsFabOpen);

        Animation mFabCloseAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.fab_close);
        Animation mFabOpenAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.fab_open);
        Animation mTextOpenAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.text_open);
        Animation mTextCloseAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.text_close);
        Animation fabAnimation, textAnimation;

        if (mIsFabOpen) {
            fabAnimation = mFabOpenAnimation;
            textAnimation = mTextOpenAnimation;
            mFragmentLibBinding.fabMenuSort.contentFab.setBackgroundColor(ColorUtils.getColorFromResource(mActivity, R.color.white_semi_transparent));
            mFragmentLibBinding.fabMenuSort.contentFab.setClickable(true);
            mFragmentLibBinding.fabMenuSort.contentFab.setFocusable(true);
            mFragmentLibBinding.fabMenuSort.contentFab.setVisibility(View.VISIBLE);
            mFragmentLibBinding.fabMenuSort.contentFab.setOnClickListener(v -> {
                mIsFabOpen = false;
                updateFabStatus();
            });

            int whiteColor = ColorUtils.getColorFromResource(mActivity, R.color.white_totally);
            int selectedColor = ColorUtils.getColorFromResource(mActivity, R.color.light_blue);

            mFragmentLibBinding.fabMenuSort.textSortDateAsc.setTextColor(whiteColor);
            mFragmentLibBinding.fabMenuSort.textSortDateDesc.setTextColor(whiteColor);
            mFragmentLibBinding.fabMenuSort.textSortSizeAsc.setTextColor(whiteColor);
            mFragmentLibBinding.fabMenuSort.textSortSizeDesc.setTextColor(whiteColor);
            mFragmentLibBinding.fabMenuSort.textSortNameAsc.setTextColor(whiteColor);
            mFragmentLibBinding.fabMenuSort.textSortNameDesc.setTextColor(whiteColor);

            switch (mCurrentSortBy) {
                case FileUtils.SORT_BY_DATE_ASC:
                    mFragmentLibBinding.fabMenuSort.textSortDateAsc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_DATE_DESC:
                    mFragmentLibBinding.fabMenuSort.textSortDateDesc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_SIZE_ASC:
                    mFragmentLibBinding.fabMenuSort.textSortSizeAsc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_SIZE_DESC:
                    mFragmentLibBinding.fabMenuSort.textSortSizeDesc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_NAME_ASC:
                    mFragmentLibBinding.fabMenuSort.textSortNameAsc.setTextColor(selectedColor);
                    break;
                case FileUtils.SORT_BY_NAME_DESC:
                    mFragmentLibBinding.fabMenuSort.textSortNameDesc.setTextColor(selectedColor);
                    break;
            }

        } else {
            fabAnimation = mFabCloseAnimation;
            textAnimation = mTextCloseAnimation;

            mFragmentLibBinding.fabMenuSort.contentFab.setBackgroundColor(ColorUtils.getColorFromResource(mActivity, R.color.transparent));
            mFragmentLibBinding.fabMenuSort.contentFab.setClickable(false);
            mFragmentLibBinding.fabMenuSort.contentFab.setFocusable(false);
            mFragmentLibBinding.fabMenuSort.contentFab.setVisibility(View.GONE);
            mFragmentLibBinding.fabMenuSort.contentFab.setOnClickListener(v -> {});
        }

        mFragmentLibBinding.fabMenuSort.fabSortDateAsc.startAnimation(fabAnimation);
        mFragmentLibBinding.fabMenuSort.fabSortDateDesc.startAnimation(fabAnimation);
        mFragmentLibBinding.fabMenuSort.fabSortNameAsc.startAnimation(fabAnimation);
        mFragmentLibBinding.fabMenuSort.fabSortNameDesc.startAnimation(fabAnimation);
        mFragmentLibBinding.fabMenuSort.fabSortSizeAsc.startAnimation(fabAnimation);
        mFragmentLibBinding.fabMenuSort.fabSortSizeDesc.startAnimation(fabAnimation);

        mFragmentLibBinding.fabMenuSort.textSortDateAsc.startAnimation(textAnimation);
        mFragmentLibBinding.fabMenuSort.textSortDateDesc.startAnimation(textAnimation);
        mFragmentLibBinding.fabMenuSort.textSortNameAsc.startAnimation(textAnimation);
        mFragmentLibBinding.fabMenuSort.textSortNameDesc.startAnimation(textAnimation);
        mFragmentLibBinding.fabMenuSort.textSortSizeAsc.startAnimation(textAnimation);
        mFragmentLibBinding.fabMenuSort.textSortSizeDesc.startAnimation(textAnimation);
    }
    
    private void setForClick() {
        if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).setClickFilterItem(view -> showFilterOptions(view));
        }
    }

    @SuppressLint("NonConstantResourceId")
    private void showFilterOptions(View view) {
        if (mIsFabOpen) {
            mIsFabOpen = false;
            updateFabStatus();
        }

        PopupMenu popup = new PopupMenu(mActivity, view);
        popup.inflate(R.menu.menu_filter_file);
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.all_type:
                    mFilterType = DataConstants.FILE_TYPE_ALL_DOCUMENT;
                    reloadData(true);
                    return false;
                case R.id.pdf_type:
                    mFilterType = DataConstants.FILE_TYPE_PDF;
                    reloadData(true);
                    return false;
                case R.id.word_type:
                    mFilterType = DataConstants.FILE_TYPE_WORD;
                    reloadData(true);
                    return false;
                case R.id.excel_type:
                    mFilterType = DataConstants.FILE_TYPE_EXCEL;
                    reloadData(true);
                    return false;
                case R.id.csv_type:
                    mFilterType = DataConstants.FILE_TYPE_CSV;
                    reloadData(true);
                    return false;
                case R.id.ppt_type:
                    mFilterType = DataConstants.FILE_TYPE_PPT;
                    reloadData(true);
                    return false;
                case R.id.txt_type:
                    mFilterType = DataConstants.FILE_TYPE_TXT;
                    reloadData(true);
                    return false;
                case R.id.rar_type:
                    mFilterType = DataConstants.FILE_TYPE_RAR;
                    reloadData(true);
                    return false;
                case R.id.zip_type:
                    mFilterType = DataConstants.FILE_TYPE_ZIP;
                    reloadData(true);
                    return false;
                default:
                    return false;
            }
        });

        CommonUtils.insertMenuItemIcons(mActivity, popup);
        popup.show();
    }

    private void setForLiveData() {
        mLibViewModel.getListFileLiveData().observe(getViewLifecycleOwner(), this::updateData);
    }

    private void updateData(List<FileData> fileDataList) {
        if (fileDataList.size() > 0) {
            if (fileDataList.equals(mListFile)) {
                mIsLoading = false;
                mFragmentLibBinding.pullToRefresh.setRefreshing(false);

                return;
            }

            mListFile = new ArrayList<>();
            mListFile.addAll(fileDataList);
            mFileListAdapter.setData(mListFile);
            showDataArea();
        } else {
            showNoDataArea();
        }

        mIsLoading = false;
        mFragmentLibBinding.pullToRefresh.setRefreshing(false);
    }
    
    private void startRequestPermission() {
        if (mActivity != null && mActivity.notHaveStoragePermission()) {
            mRequestPermissionDialog = DialogFactory.getDialogRequestSomething(mActivity, getString(R.string.title_need_permission), getString(R.string.need_permission_to_get_file));
            mRequestPermissionDialog.setConfirmClickListener(sweetAlertDialog -> {
                requestReadStoragePermissionsSafely(REQUEST_EXTERNAL_PERMISSION_FOR_LOAD_FILE_CODE);
            });
            mRequestPermissionDialog.setCancelClickListener(sweetAlertDialog -> {
                sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(getString(R.string.title_need_permission_fail));
                sweetAlertDialog.setContentText(getString(R.string.couldnt_get_file_now));
                sweetAlertDialog.setConfirmClickListener(Dialog::dismiss);
                sweetAlertDialog.showCancelButton(false);
                sweetAlertDialog.setConfirmText(getString(R.string.confirm_text));
            });
            mRequestPermissionDialog.show();
        } else {
            reloadData(true);
        }
    }

    private void showNoDataArea() {
        mFragmentLibBinding.noDataErrorTv.setText(R.string.no_file_found);

        mFragmentLibBinding.dataListArea.setVisibility(View.GONE);
        mFragmentLibBinding.noDataErrorArea.setVisibility(View.VISIBLE);
        mFragmentLibBinding.noPermissionArea.setVisibility(View.GONE);
        mFragmentLibBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showPermissionIssueArea() {
        mFragmentLibBinding.noPermissionArea.setOnClickListener(v -> {
            startRequestPermission();
        });
        mFragmentLibBinding.dataListArea.setVisibility(View.GONE);
        mFragmentLibBinding.noDataErrorArea.setVisibility(View.GONE);
        mFragmentLibBinding.noPermissionArea.setVisibility(View.VISIBLE);
        mFragmentLibBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showDataArea() {
        mFragmentLibBinding.dataListArea.setVisibility(View.VISIBLE);
        mFragmentLibBinding.noDataErrorArea.setVisibility(View.GONE);
        mFragmentLibBinding.noPermissionArea.setVisibility(View.GONE);
        mFragmentLibBinding.loadingArea.setVisibility(View.GONE);
    }

    private void showLoadingArea() {
        mFragmentLibBinding.dataListArea.setVisibility(View.GONE);
        mFragmentLibBinding.noDataErrorArea.setVisibility(View.GONE);
        mFragmentLibBinding.loadingArea.setVisibility(View.VISIBLE);
        mFragmentLibBinding.noPermissionArea.setVisibility(View.GONE);
    }

    @Override
    public void onClickItem(int position) {
        String filePath = mListFile.get(position).getFilePath();
        if (filePath == null) {
            filePath = RealPathUtil.getInstance().getRealPath(mActivity, mListFile.get(position).getFileUri());
        }

        String finalFilePath = filePath;
        if (mActivity != null) {
            mActivity.showViewFileAdsBeforeAction(() -> {
                openDocumentFile(finalFilePath);
                mFileListAdapter.setCurrentItem(position);
            });
        }
    }

    @Override
    public void onMainFunctionItem(int position) {
        onShowOption(position);
    }

    @Override
    public void onOptionItem(int position) {
        onShowOption(position);
    }

    private void onShowOption(int position) {
        FileData fileData = mListFile.get(position);

        hideOptionDialog();
        fileOptionDialog = new FileOptionDialog(fileData.getDisplayName(), fileData.getTimeAdded(), position, new FileOptionDialog.FileOptionListener() {
            @Override
            public void openFile(int position) {
                openPdfFile(position);
            }

            @Override
            public void shareFile(int position) {
                shareDocumentFile(fileData.getFilePath());
            }

            @Override
            public void printFile(int position) {
                printDocumentFile(fileData.getFilePath());
            }

            @Override
            public void infoFile(int position) {
                infoDocumentFile(fileData.getFilePath());
            }

            @Override
            public void renameFile(int position) {
                renamePdfFile(position);
            }

            @Override
            public void deleteFile(int position) {
                deletePdfFile(position);
            }
        });
        fileOptionDialog.show(getChildFragmentManager(), fileOptionDialog.getTag());
        CommonUtils.hideKeyboard(mActivity);
    }

    private void hideOptionDialog() {
        if (fileOptionDialog != null && fileOptionDialog.isVisible()) {
            fileOptionDialog.dismiss();
        }
    }

    private void openPdfFile(int position) {
        onClickItem(position);
    }

    private void renamePdfFile(int position) {
        FirebaseUtils.sendEventFunctionUsed(mActivity, FirebaseUtils.EVENT_NAME_LIST_FILE, "Rename file");

        FileData fileData = mListFile.get(position);
        String displayName;
        String extension;
        try {
            displayName = fileData.getDisplayName().substring(0, fileData.getDisplayName().lastIndexOf("."));
            extension = fileData.getDisplayName().substring(fileData.getDisplayName().lastIndexOf("."));
        } catch (Exception e) {
            return;
        }

        RenameFileDialog renameFileDialog = new RenameFileDialog(mActivity, displayName, new RenameFileDialog.RenameFileListener() {
            @Override
            public void onSubmitName(String name) {
                String newName = name + extension;
                int result = FileUtils.renameFile(fileData, newName);

                if (result == -2 || result == 0) {
                    ToastUtils.showMessageShort(mActivity, getString(R.string.can_not_edit_video_name));
                } else if (result == -1) {
                    SnackBarUtils.getSnackbar(mActivity, getString(R.string.duplicate_video_name) + ": " + name).show();
                } else {
                    SnackBarUtils.getSnackbar(mActivity, getString(R.string.rename_file_success)).show();
                    String oldFilePath = fileData.getFilePath();

                    fileData.setFilePath(fileData.getFilePath().replace(fileData.getDisplayName(), newName));
                    fileData.setDisplayName(newName);
                    mListFile.set(position, fileData);
                    mFileListAdapter.updateData(position, fileData);

                    mLibViewModel.updateSavedData(oldFilePath, fileData.getFilePath());
                }
            }

            @Override
            public void onCancel() {

            }
        });

        renameFileDialog.show();
    }

    private void deletePdfFile(int position) {
        FirebaseUtils.sendEventFunctionUsed(mActivity, FirebaseUtils.EVENT_NAME_LIST_FILE, "Delete file");

        FileData fileData = mListFile.get(position);

        ConfirmDialog confirmDialog = new ConfirmDialog(mActivity, mActivity.getString(R.string.confirm_delete_file_title), mActivity.getString(R.string.confirm_delete_file_message), new ConfirmDialog.ConfirmListener() {
            @Override
            public void onSubmit() {
                if (mActivity != null && !mActivity.notHaveStoragePermission()) {
                    FileUtils.deleteFileOnExist(fileData.getFilePath());
                    mLibViewModel.clearSavedData(fileData.getFilePath());

                    if (position >= 0 && position < mListFile.size()) {
                        mListFile.remove(position);
                    }

                    mFileListAdapter.clearData(position);
                    if (mListFile.size() == 0) {
                        showNoDataArea();
                    }
                    SnackBarUtils.getSnackbar(mActivity, mActivity.getString(R.string.delete_success_text)).show();
                    hideOptionDialog();
                }
            }

            @Override
            public void onCancel() {

            }
        });
        confirmDialog.show();
    }
}
