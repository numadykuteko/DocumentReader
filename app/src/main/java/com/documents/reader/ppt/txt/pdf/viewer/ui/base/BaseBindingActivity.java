package com.documents.reader.ppt.txt.pdf.viewer.ui.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.provider.Settings;

import androidx.annotation.IntegerRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.ads.control.Admod;
import com.ads.control.funtion.AdCallback;
import com.documents.reader.ppt.txt.pdf.viewer.ui.viewzip.ZipViewerActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ads.AdsShowCountMyPdfManager;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ads.FileViewerOnceManager;
import com.google.android.gms.ads.InterstitialAd;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.AppConstants;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.DataManager;
import com.documents.reader.ppt.txt.pdf.viewer.ui.main.MainActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.search.SearchActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.splash.SplashActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.viewoffice.OfficeReaderActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.viewpdf.ViewPdfActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DateTimeUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.DialogFactory;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.NetworkUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.DirectoryUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;
import com.rate.control.OnCallback;
import com.rate.control.funtion.RateUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

public abstract class BaseBindingActivity<T extends ViewDataBinding, V extends BaseViewModel>
        extends AppCompatActivity implements BaseFragment.Callback {

    protected static final int PREVIEW_FILE_REQUEST = 2369;
    protected static final int TAKE_FILE_REQUEST = 2365;

    public static final String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    protected static final String EXTRA_IS_PREVIEW = "EXTRA_IS_PREVIEW";
    protected static final String EXTRA_FROM_FIRST_OPEN = "EXTRA_FROM_FIRST_OPEN";

    protected static final int RESULT_FILE_DELETED = -1111;
    public static final int RESULT_NEED_FINISH = -1112;

    private static final String TAG = "BaseBindingActivity";
    private V mViewModel;
    private T mViewDataBinding;

    private SweetAlertDialog mDownloadFromGgDriveDialog;

    private InterstitialAd mViewFileInterstitialAd;
    private InterstitialAd mExitInterstitialAd;

    protected boolean mIsRequestFullPermission = false;
    protected int mRequestFullPermissionCode = -1000;

    protected boolean mIsNeedSetTheme = true;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    @Override
    public void onBackPressed() {
//        if (isTaskRoot() && !(this instanceof MainActivity)) {
//            restartApp(false);
//            return;
//        }
        super.onBackPressed();
    }

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    @Override
    public void onFragmentAttached() {
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mIsNeedSetTheme) {
            int chosenTheme = DataManager.getInstance(this).getTheme();
            int[] THEME_LIST = {R.style.RedAppTheme, R.style.BlueAppTheme, R.style.JadeAppTheme, R.style.VioletAppTheme, R.style.OrangeAppTheme, R.style.GreenAppTheme, R.style.YellowThemeColor};

            int selectedTheme = THEME_LIST[chosenTheme];

            setTheme(selectedTheme);
        }

        setNoActionBar();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        performDataBinding();

        String appLocaleString = DataManager.getInstance(this).getAppLocale();
        Locale appLocale;

        if (appLocaleString == null) {
            appLocale = getResources().getConfiguration().locale;
        } else {
            appLocale = new Locale(appLocaleString);
        }

        Locale.setDefault(appLocale);
        Configuration config = new Configuration();
        config.locale = appLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }


    public void preloadMyPdfAdsIfInit() {
        mViewFileInterstitialAd = Admod.getInstance().getInterstitalAds(this, BuildConfig.full_view_file_id);
    }

    public void preloadExitAdsIfInit() {
        mExitInterstitialAd = Admod.getInstance().getInterstitalAds(this, BuildConfig.full_exit_id);
    }

    public void showOnePerTwoTapFunctionAdsBeforeAction(Runnable callback) {
        callback.run();
    }

    public void showViewFileAdsBeforeAction(Runnable callback) {
        if (AdsShowCountMyPdfManager.getInstance().checkShowAdsForClickItem()) {
            Admod.getInstance().forceShowInterstitial(this, mViewFileInterstitialAd, new AdCallback() {
                @Override
                public void onAdClosed() {
                    callback.run();
                }
            });
        } else {
            callback.run();
        }
        AdsShowCountMyPdfManager.getInstance().increaseCountForClickItem();
    }

    public void showExitAppAds(Runnable callback) {
        Admod.getInstance().forceShowInterstitial(this, mExitInterstitialAd, new AdCallback() {
            @Override
            public void onAdClosed() {
                callback.run();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * create view component
     */
    protected abstract void initView();

    /**
     * set on-click listener for view component
     */
    protected abstract void setClick();

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    public void requestReadStoragePermissionsSafely(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            mIsRequestFullPermission = true;
            mRequestFullPermissionCode = requestCode;

            startActivity(intent);
        }
    }

    public void requestFullStoragePermission() {

    }

    public boolean notHaveStoragePermission() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            return (!hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) && !hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE));
        } else {
            return (!Environment.isExternalStorageManager());
        }
    }

    @SuppressLint("RestrictedApi")
    public void setNoActionBar() {
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setShowHideAnimationEnabled(false);

            actionBar.hide();
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @SuppressLint("RestrictedApi")
    public void setActionBar(String title, boolean isShowBackButton) {
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setShowHideAnimationEnabled(false);

            actionBar.show();
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(title);

            actionBar.setHomeButtonEnabled(isShowBackButton);
            actionBar.setDisplayHomeAsUpEnabled(isShowBackButton);
        }
    }

    public void restartApp(boolean isFromSplash) {
        Intent intent;
        if (isFromSplash) {
            intent = new Intent(BaseBindingActivity.this, SplashActivity.class);
        } else {
            intent = new Intent(BaseBindingActivity.this, MainActivity.class);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);
        finish();
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public int getIntegerByResource(@IntegerRes int integer) {
        return getResources().getInteger(integer);
    }

    /**
     * for set full screen without action bar and navigation bar()
     */
    protected void setActivityFullScreen() {

    }

    protected void setActivityWithActionBar() {

    }

    /**
     * Show popup download from Google drive
     */

    protected void updateFilePathFromGGDrive(Uri uri, String filePath) {
        try {
            if (mDownloadFromGgDriveDialog != null && mDownloadFromGgDriveDialog.isShowing()) {
                mDownloadFromGgDriveDialog.dismiss();
            }
        } catch (Exception e) {
            // donothing
        }
    }

    protected void startDownloadFromGoogleDrive(Uri uri) {
        mDownloadFromGgDriveDialog = DialogFactory.getDialogProgress(this, getString(R.string.downloading_from_gg_drive_text));
        mDownloadFromGgDriveDialog.show();

        AsyncTask.execute(() -> {
            try {
                @SuppressLint("Recycle")
                Cursor returnCursor = getContentResolver().query(uri, null, null, null, null);

                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                returnCursor.moveToFirst();
                String originalName = (returnCursor.getString(nameIndex));
                String size = (Long.toString(returnCursor.getLong(sizeIndex)));

                if (originalName == null) {
                    originalName = getString(R.string.prefix_for_google_drive) + DateTimeUtils.currentTimeToNaming();
                }

                File file = new File(DirectoryUtils.getDefaultStorageLocation(), originalName);
                InputStream inputStream = getContentResolver().openInputStream(uri);
                FileOutputStream outputStream = new FileOutputStream(file);
                int read = 0;
                int maxBufferSize = 1024 * 1024;
                int bytesAvailable = inputStream.available();

                int bufferSize = Math.min(bytesAvailable, maxBufferSize);

                final byte[] buffers = new byte[bufferSize];
                while ((read = inputStream.read(buffers)) != -1) {
                    outputStream.write(buffers, 0, read);
                }
                inputStream.close();
                outputStream.close();

                runOnUiThread(() -> {
                    updateFilePathFromGGDrive(uri, file.getPath());
                });

            } catch (Exception e) {
                runOnUiThread(() -> {
                    updateFilePathFromGGDrive(uri, null);
                });
            }
        });
    }

    protected void updateFilePathFromGGDriveList(int index, ArrayList<Uri> uriList, String filePath) {
        if (mDownloadFromGgDriveDialog != null) {
            if (index == uriList.size() - 1)
                mDownloadFromGgDriveDialog.dismiss();
        }
    }

    protected void startDownloadFromGoogleDriveList(ArrayList<Uri> uriList) {
        runOnUiThread(() -> {
            mDownloadFromGgDriveDialog = DialogFactory.getDialogProgress(this, getString(R.string.downloading_from_gg_drive_text));
            mDownloadFromGgDriveDialog.show();
        });

        for (int i = 0; i < uriList.size(); i++) {
            int finalIndex = i;
            Uri uri = uriList.get(i);
            if (uri == null) {
                runOnUiThread(() -> {
                    updateFilePathFromGGDriveList(finalIndex, uriList, null);
                });
                return;
            }

            try {
                @SuppressLint("Recycle")
                Cursor returnCursor = getContentResolver().query(uri, null, null, null, null);

                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                returnCursor.moveToFirst();
                String originalName = (returnCursor.getString(nameIndex));
                String size = (Long.toString(returnCursor.getLong(sizeIndex)));

                if (originalName == null) {
                    originalName = getString(R.string.prefix_for_google_drive) + DateTimeUtils.currentTimeToNaming();
                }

                File file = new File(DirectoryUtils.getDefaultStorageLocation(), originalName);
                InputStream inputStream = getContentResolver().openInputStream(uri);
                FileOutputStream outputStream = new FileOutputStream(file);
                int read = 0;
                int maxBufferSize = 1024 * 1024;
                int bytesAvailable = inputStream.available();

                int bufferSize = Math.min(bytesAvailable, maxBufferSize);

                final byte[] buffers = new byte[bufferSize];
                while ((read = inputStream.read(buffers)) != -1) {
                    outputStream.write(buffers, 0, read);
                }
                inputStream.close();
                outputStream.close();

                runOnUiThread(() -> {
                    updateFilePathFromGGDriveList(finalIndex, uriList, file.getPath());
                });
            } catch (Exception e) {
                runOnUiThread(() -> {
                    updateFilePathFromGGDriveList(finalIndex, uriList, null);
                });
            }
        }
    }

    /**
     * for activity move - all activity moving should put here - ads coverage
     */

    public void gotoActivityWithFlag(int flag) {
        Intent intent;

        switch (flag) {

            case AppConstants.FLAG_SEARCH_PDF:
                intent = new Intent(BaseBindingActivity.this, SearchActivity.class);
                startActivity(intent);
                FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_HOME, "click_Search");

                break;
            case AppConstants.FLAG_VIEW_PDF:
                showOnePerTwoTapFunctionAdsBeforeAction(() -> {
                    Intent viewPdfIntent = new Intent(BaseBindingActivity.this, ViewPdfActivity.class);
                    startActivity(viewPdfIntent);
                });

                break;
        }
    }

    public void gotoViewByAndroidViewActivity(String filePath, String fileType) {
        if (filePath != null) {
            FileUtils.openFile(this, filePath, fileType);
        } else {
            ToastUtils.showMessageShort(this, getString(R.string.can_not_open_file));
        }
    }

    protected void openDocumentFile(String filePath) {
        if (filePath == null) {
            return;
        }

        String fileType = FileUtils.getFileType(new File(filePath));
        if (fileType.equals(DataConstants.FILE_TYPE_PDF)) {
            gotoPdfFileViewActivity(filePath);
        } else if (fileType.equals(DataConstants.FILE_TYPE_RAR) || fileType.equals(DataConstants.FILE_TYPE_ZIP)) {
            gotoZipFileViewActivity(filePath);
        } else if (fileType.length() > 0) {
            gotoDocumentFileViewActivity(filePath);
        } else {
            ToastUtils.showMessageShort(this, getString(R.string.file_not_supported));
        }
    }

    public void gotoPdfFileViewActivity(String filePath) {
        FileViewerOnceManager.getInstance().setFileViewerOnce();

        Intent intent = new Intent(BaseBindingActivity.this, ViewPdfActivity.class);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        startActivity(intent);

        FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_LIST_FILE, "View Pdf");
    }

    public void gotoZipFileViewActivity(String filePath) {
        FileViewerOnceManager.getInstance().setFileViewerOnce();

        Intent intent = new Intent(BaseBindingActivity.this, ZipViewerActivity.class);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        startActivity(intent);

        FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_LIST_FILE, "View Zip");
    }

    public void gotoDocumentFileViewActivity(String filePath) {
        FileViewerOnceManager.getInstance().setFileViewerOnce();

        Intent intent = new Intent(BaseBindingActivity.this, OfficeReaderActivity.class);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        startActivity(intent);

        FirebaseUtils.sendEventFunctionUsed(this, FirebaseUtils.EVENT_NAME_LIST_FILE, "View Document");
    }
    
    /**
     * Request rating
     */

    @SuppressLint("ResourceAsColor")
    public void showRatingUsPopup(final Runnable positiveRunnable, final Runnable negativeRunnable, final Runnable rejectRunnable) {
        RateUtils.showRateDialog(this, new OnCallback() {
            @Override
            public void onMaybeLater() {
                rejectRunnable.run();
            }

            @Override
            public void onSubmit(String review) {
                FirebaseUtils.sendEventFunctionUsed(BaseBindingActivity.this, FirebaseUtils.EVENT_NAME_RATE, "1-3 stars", review);
                ToastUtils.showMessageShort(BaseBindingActivity.this, getString(R.string.thank_you_for_rate_us));
                DataManager dataManager = DataManager.getInstance(getApplicationContext());
                dataManager.setRatingUsDone();
                negativeRunnable.run();
            }

            @Override
            public void onRate() {
                FirebaseUtils.sendEventFunctionUsed(BaseBindingActivity.this, FirebaseUtils.EVENT_NAME_RATE, "4-5 stars");
                gotoRateUsActivity();
                positiveRunnable.run();
            }
        });
    }

    public void gotoRateUsActivity() {
        DataManager dataManager = DataManager.getInstance(this);
        dataManager.setRatingUsDone();

        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }
}
