package com.documents.reader.ppt.txt.pdf.viewer.ui.search;

import android.content.Context;
import android.os.AsyncTask;

import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.util.List;

public class SearchFileAsyncTask extends AsyncTask<Object, Object, Object> {

    private final Context mContext;
    private SearchFileListener mListener;
    private boolean mIsSearchAll = DataConstants.USE_DEEP_SEARCH;

    public SearchFileAsyncTask(Context context, SearchFileListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            List<FileData> resultAll;
            if (mIsSearchAll) {
                resultAll = FileUtils.getAllExternalFileList(mContext, DataConstants.FILE_TYPE_ALL_DOCUMENT, FileUtils.SORT_BY_DATE_DESC);
            } else {
                resultAll = FileUtils.getExternalFileList(mContext, DataConstants.FILE_TYPE_ALL_DOCUMENT, FileUtils.SORT_BY_DATE_DESC);
            }

            if (!isCancelled() && mListener != null) {
                mListener.loadDone(resultAll);
            }
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}
