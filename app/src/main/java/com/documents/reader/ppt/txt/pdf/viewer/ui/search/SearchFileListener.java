package com.documents.reader.ppt.txt.pdf.viewer.ui.search;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;

import java.util.List;

public interface SearchFileListener {
    public void loadDone(List<FileData> result);
}
