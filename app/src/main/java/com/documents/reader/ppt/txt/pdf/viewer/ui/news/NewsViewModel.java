package com.documents.reader.ppt.txt.pdf.viewer.ui.news;

import android.app.Application;

import androidx.annotation.NonNull;

import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;

public class NewsViewModel extends BaseViewModel<NewsNavigator> {
    public NewsViewModel(@NonNull Application application) {
        super(application);
    }
}
