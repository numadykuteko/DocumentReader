package com.documents.reader.ppt.txt.pdf.viewer.ui.main;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.documents.reader.ppt.txt.pdf.viewer.R;

import java.util.Timer;
import java.util.TimerTask;

public class AfterAdsActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_after_ads);

        exitAfterOneSecond();
    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
    }

    private void exitAfterOneSecond() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                finishAndRemoveTask();
            }
        };
        Timer timer = new Timer();
        timer.schedule(timerTask, 1200);
    }
}
