package com.documents.reader.ppt.txt.pdf.viewer.ui.news;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.FragmentNewsBinding;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseFragment;

public class NewsFragment extends BaseFragment<FragmentNewsBinding, NewsViewModel> implements NewsNavigator {

    private NewsViewModel mNewsViewModel;
    private FragmentNewsBinding mFragmentNewsBinding;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_news;
    }

    @Override
    public void reloadEasyChangeData() {

    }

    @Override
    public NewsViewModel getViewModel() {
        mNewsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        return mNewsViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mNewsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentNewsBinding = getViewDataBinding();

        setForClick();
        initView();
        setForLiveData();
    }

    public static NewsFragment newInstance() {
        NewsFragment libFragment = new NewsFragment();

        Bundle args = new Bundle();
        libFragment.setArguments(args);
        libFragment.setRetainInstance(true);

        return libFragment;
    }

    private void initView() {
    }

    private void setForClick() {

    }

    private void setForLiveData() {
    }
}