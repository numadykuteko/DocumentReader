package com.documents.reader.ppt.txt.pdf.viewer.ui.viewzip;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.documents.reader.ppt.txt.pdf.viewer.data.model.FileData;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseViewModel;
import com.documents.reader.ppt.txt.pdf.viewer.ui.browser.BrowserFileAsyncTask;

import java.util.ArrayList;
import java.util.List;

public class ZipViewerViewModel extends BaseViewModel<ZipViewerNavigator> {
    private MutableLiveData<String> mZipPathLiveData = new MutableLiveData<>();
    public MutableLiveData<String> getZipPathLiveData() {
        return mZipPathLiveData;
    }

    private FileData mCurrentPath;
    private BrowserFileAsyncTask mAsyncTask;
    private List<FileData> mListFile;
    private MutableLiveData<List<FileData>> mListFileLiveData = new MutableLiveData<>();
    public MutableLiveData<List<FileData>> getListFileLiveData() {
        return mListFileLiveData;
    }

    public ZipViewerViewModel(@NonNull Application application) {
        super(application);
    }

    public void startExactZipFile(String filePath) {
        ExactFileAsyncTask exactFileAsyncTask = new ExactFileAsyncTask(getApplication(), result -> mZipPathLiveData.postValue(result), filePath);

        exactFileAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setCurrentPath(FileData currentPath) {
        this.mCurrentPath = currentPath;
    }

    public void getFileList() {
        mAsyncTask = new BrowserFileAsyncTask(getApplication(), result -> {
            mListFile = new ArrayList<>(result);

            mListFileLiveData.postValue(mListFile);
        }, mCurrentPath);
        mAsyncTask.setIsScanAll(true);
        mAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
