package com.documents.reader.ppt.txt.pdf.viewer.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataConstants {

    public static final String DATABASE_NAME = "pdf_reader_application";
    public static final String PREF_NAME = "pdf_reader_application";

    public static final String EMAIL_DEV = "elaineeyui@gmail.com";

    public static final int CONNECT_TIMEOUT_NETWORK = 10;

    public static final String PREF_NAME_OPEN_BEFORE = "PREF_NAME_OPEN_BEFORE";
    public static final String PREF_NAME_SHOW_GUIDE_CONVERT = "PREF_NAME_SHOW_GUIDE_CONVERT";
    public static final String PREF_NAME_RATING_US = "PREF_NAME_RATING_US";
    public static final String PREF_NAME_SHOW_GUIDE = "PREF_NAME_SHOW_GUIDE";
    public static final String PREF_NAME_THEME = "PREF_NAME_THEME";
    public static final String PREF_NAME_BACK_TIME = "PREF_NAME_BACK_TIME";
    public static final String PREF_NAME_OPTION_VIEW_PDF = "PREF_NAME_OPTION_VIEW_PDF";
    public static final String PREF_NAME_APP_LOCALE = "PREF_NAME_APP_LOCALE";
    public static final String PREF_NAME_OPEN_FROM_OTHER_APP = "PREF_NAME_OPEN_FROM_OTHER_APP";

    public static final String IMAGE_DESCRIPTION = "Image provided by " + DATABASE_NAME;

    public static final int MAX_SIZE_IMAGE_LOADER_WIDTH = 200;
    public static final int MAX_SIZE_IMAGE_LOADER_HEIGHT = 300;

    public static final int HORIZONTAL_IMAGE_LOADER_WIDTH = 300;
    public static final int HORIZONTAL_IMAGE_LOADER_HEIGHT = 200;

    public static final String FILE_TYPE_PDF = "pdf";
    public static final String FILE_TYPE_WORD = "doc";
    public static final String FILE_TYPE_TXT = "txt";
    public static final String FILE_TYPE_EXCEL = "xlsx";
    public static final String FILE_TYPE_CSV = "csv";
    public static final String FILE_TYPE_PPT = "ppt";
    public static final String FILE_TYPE_RTF = "rtf";
    public static final String FILE_TYPE_RAR = "rar";
    public static final String FILE_TYPE_ZIP = "zip";
    public static final String FILE_TYPE_DIRECTORY = "__directory";
    public static final String FILE_TYPE_ALL_DOCUMENT = "__all document";

    public static final String PDF_EXTENSION = ".pdf";
    public static final String TEXT_EXTENSION = ".txt";
    public static final String EXCEL_EXTENSION = ".xls";
    public static final String EXCEL_WORKBOOK_EXTENSION = ".xlsx";
    public static final String EXCEL_CSV_EXTENSION = ".csv";
    public static final String DOC_EXTENSION = ".doc";
    public static final String DOCX_EXTENSION = ".docx";
    public static final String PPT_EXTENSION = ".ppt";
    public static final String PPTX_EXTENSION = ".pptx";
    public static final String POT_EXTENSION = ".pot";
    public static final String PPTM_EXTENSION = ".pptm";
    public static final String POTX_EXTENSION = ".potx";
    public static final String POTM_EXTENSION = ".potm";
    public static final String RTF_EXTENSION = ".rtf";
    public static final String RAR_EXTENSION = ".rar";
    public static final String ZIP_EXTENSION = ".zip";

    public static final List<String> ALL_EXTENSIONS = Arrays.asList(DataConstants.PDF_EXTENSION,
            DataConstants.EXCEL_EXTENSION, DataConstants.EXCEL_WORKBOOK_EXTENSION, DataConstants.EXCEL_CSV_EXTENSION,
            DataConstants.PPT_EXTENSION, DataConstants.PPTX_EXTENSION, DataConstants.POT_EXTENSION, DataConstants.PPTM_EXTENSION, DataConstants.POTX_EXTENSION, DataConstants.POTM_EXTENSION,
            DataConstants.DOC_EXTENSION, DataConstants.DOCX_EXTENSION,
            DataConstants.TEXT_EXTENSION,
            DataConstants.RTF_EXTENSION,
            DataConstants.RAR_EXTENSION,
            DataConstants.ZIP_EXTENSION);

    public static final String PDF_DIRECTORY = "/PDFfiles/";
    public static final String IMAGE_DIRECTORY = "/ImageFiles/";

    public static final boolean USE_DEEP_SEARCH = true;

    public static final String TEMP_FOLDER = "Temp_folder_123123";

    public static final int VIEW_MODE_DAY = 0;
    public static final int VIEW_MODE_NIGHT = 1;
    public static final int VIEW_ORIENTATION_VERTICAL = 0;
    public static final int VIEW_ORIENTATION_HORIZONTAL = 1;

    public static final String ALL_DOCUMENTS = "                 _________All documents";
}
