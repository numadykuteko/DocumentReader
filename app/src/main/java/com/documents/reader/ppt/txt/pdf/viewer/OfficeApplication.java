package com.documents.reader.ppt.txt.pdf.viewer;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;

public class OfficeApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        MobileAds.initialize(
                this,
                initializationStatus -> {});
    }
}
