package com.documents.reader.ppt.txt.pdf.viewer.ui.component;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.utils.ToastUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileInfoUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;

import java.io.File;

public class FileInfoDialog extends Dialog {

    private Context mContext;
    private ConfirmListener mListener;

    public FileInfoDialog(@NonNull Context context, ConfirmListener listener, String filePath) {
        super(context);
        mContext = context;
        mListener = listener;

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_file_info);

        int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.968);
        getWindow().setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);

        File file = new File(filePath);
        if (!file.exists()) {
            ToastUtils.showMessageShort(getContext(), getContext().getString(R.string.file_not_found));
            dismiss();
            return;
        }

        TextView nameFile = findViewById(R.id.name_file);
        TextView pathFile = findViewById(R.id.path_file);
        TextView sizeFile = findViewById(R.id.size_file);
        TextView dateFile = findViewById(R.id.date_file);

        nameFile.setText(FileUtils.getFileName(filePath));
        pathFile.setText(filePath);
        sizeFile.setText(FileInfoUtils.getFormattedSize(file));
        String formattedDate = FileInfoUtils.getFormattedDate(file);
        if (formattedDate.length() > 0) {
            dateFile.setText(formattedDate);
        } else {
            dateFile.setText("N/A");
        }

        Button cancel = findViewById(R.id.btn_cancel);
        Button open = findViewById(R.id.btn_open);

        cancel.setOnClickListener(v -> {
            dismiss();
        });

        open.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onOpen();
            }
            dismiss();
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public interface ConfirmListener {
        void onOpen();
    }
}
