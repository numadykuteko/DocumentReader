package com.documents.reader.ppt.txt.pdf.viewer.ui.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.ads.control.Admod;
import com.ads.control.funtion.AdCallback;
import com.documents.reader.ppt.txt.pdf.viewer.BuildConfig;
import com.documents.reader.ppt.txt.pdf.viewer.R;
import com.documents.reader.ppt.txt.pdf.viewer.constants.DataConstants;
import com.documents.reader.ppt.txt.pdf.viewer.data.DataManager;
import com.documents.reader.ppt.txt.pdf.viewer.databinding.ActivitySplashBinding;
import com.documents.reader.ppt.txt.pdf.viewer.ui.base.BaseBindingActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.main.MainActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.viewoffice.OfficeReaderActivity;
import com.documents.reader.ppt.txt.pdf.viewer.ui.viewpdf.ViewPdfActivity;
import com.documents.reader.ppt.txt.pdf.viewer.utils.FirebaseRemoteUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.NetworkUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.FileUtils;
import com.documents.reader.ppt.txt.pdf.viewer.utils.file.RealPathUtil;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseBindingActivity<ActivitySplashBinding, SplashViewModel> implements SplashNavigator {
    private SplashViewModel mSplashViewModel;
    private ActivitySplashBinding mActivitySplashBinding;

    private boolean mIsFromOtherApp = false;
    private String mFilePath = null;
    private String mFileType = null;

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        mSplashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        return null;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mIsNeedSetTheme = false;
        super.onCreate(savedInstanceState);

        mActivitySplashBinding = getViewDataBinding();
        mSplashViewModel.setNavigator(this);

        FirebaseRemoteUtils firebaseRemoteUtils = new FirebaseRemoteUtils();
        firebaseRemoteUtils.fetchRemoteConfig(this, () -> {});
        precheckIntentFilter();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void setClick() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    private void gotoTargetActivity() {
        Intent intent;
        if (!mIsFromOtherApp || mFilePath == null) {
            intent = new Intent(SplashActivity.this, MainActivity.class);
        } else {
            if (mFileType.equals(DataConstants.FILE_TYPE_PDF)) {
                intent = new Intent(SplashActivity.this, ViewPdfActivity.class);
            } else {
                intent = new Intent(SplashActivity.this, OfficeReaderActivity.class);
            }
            intent.putExtra(EXTRA_FILE_PATH, mFilePath);
            intent.putExtra(EXTRA_FROM_FIRST_OPEN, true);
        }
        // TODO to set reopen by click icon set clear task and launch mode singleTask
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void precheckIntentFilter() {
        Intent intent = getIntent();

        if (intent != null) {
            String action = intent.getAction();
            String type = intent.getType();
            String filepath = null;

            if (Intent.ACTION_VIEW.equals(action) || Intent.ACTION_SEND.equals(action)) {
                Uri fileUri = intent.getData();
                if (fileUri != null) {
                    filepath = RealPathUtil.getInstance().getRealPath(this, fileUri);
                }
            }

            if (FileUtils.checkFileExist(filepath)) {
                mFileType = FileUtils.getFileType(new File(filepath));
                if (mFileType != null && mFileType.length() > 0) {
                    mIsFromOtherApp = true;
                    mFilePath = filepath;
                }
            }
        } else {
            mIsFromOtherApp = false;
        }

        prepareShowAds();
    }

    private void prepareShowAds() {
        if (!NetworkUtils.isNetworkConnected(this)) {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    gotoTargetActivity();
                }
            }, 1000);
            return;
        }

        int numberTimeOpenOtherApp = DataManager.getInstance(this).getOpenFromOtherAppTime();
        FirebaseRemoteUtils firebaseRemoteUtils = new FirebaseRemoteUtils();

        if (mIsFromOtherApp) {
            if (numberTimeOpenOtherApp >= firebaseRemoteUtils.getNumberTimeShowAdsOnce()) {
                Admod.getInstance().loadSplashInterstitalAds(this,
                        BuildConfig.full_splash_from_other_id,
                        (int) firebaseRemoteUtils.getTimeoutSplash(),
                        new AdCallback() {
                            @Override
                            public void onAdClosed() {
                                gotoTargetActivity();
                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                gotoTargetActivity();
                            }
                        });
                DataManager.getInstance(this).setOpenFromOtherAppTime(1);
            } else {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        gotoTargetActivity();
                    }
                }, 1000);
                DataManager.getInstance(this).setOpenFromOtherAppTime(numberTimeOpenOtherApp + 1);
            }
        } else {
            Admod.getInstance().loadSplashInterstitalAds(this,
                    BuildConfig.full_splash_id,
                    (int) firebaseRemoteUtils.getTimeoutSplash(),
                    new AdCallback() {
                        @Override
                        public void onAdClosed() {
                            gotoTargetActivity();
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            gotoTargetActivity();
                        }
                    });
        }
    }
}
